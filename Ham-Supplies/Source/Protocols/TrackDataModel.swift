//
//  TrackDataModel.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/24/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation
import Disk

protocol TrackSearchable {
  var searchString: String { get }
}

protocol TrackDataModel: TrackModel, TrackSearchable { }

protocol TrackModel {
  var id: String { get set }
  var time: Int { get set }
  var year: Int { get set }
  var bpm: Int { get set }
  var key: String { get set }
  var energy: String { get set }
  var bitrate: Int { get set }
  var artist: String { get set }
  var title: String { get set }
  var album: String { get set }
  var label: String { get set }
  var genre: String { get set }
  var released: String { get set }
  var added: String { get set }
  var file: String { get set }
  var artwork: String { get set }

  var dateAdded: Date? { get set }
  var dateReleased: Date? { get set }
}

extension TrackSearchable where Self: TrackModel {
  var searchString: String {
    return "\(title) \(artist) \(album) \(genre) \(year)"
  }
}

extension TrackDataModel {

  var musicFileType: String? {
    if let songType = file.split(separator: ".").last {
      return String(songType)
    }
    return nil
  }

  var minutes: Int {
    return Int(time / 60)
  }

  var seconds: Float {
    return Float(time).truncatingRemainder(dividingBy: 60)
  }

  var formatDuration: String {
    return "\(minutes):\(seconds.secondsString)"
  }

  var offlineFileName: String {
    if let fileType = musicFileType {
      return "\(id).\(fileType)"
    } else {
      return file
    }
  }

  var convertedFileName: String {
    return "\(id).m4a"
  }

  var offlineArtworkFileName: String {
    if let fileName = artwork.split(separator: "/").last {
      return String(fileName)
    } else {
      return artwork
    }
  }

  var songRemoteURL: URL? {
    return Router.buildURL(forPath: file)
  }

  var artworkRemoteURL: URL? {
    return Router.buildURL(forPath: artwork)
  }

  var songDiskResults: DiskSearchResult {
    return Disk.search(forFile: offlineFileName)
  }

  var artworkDiskResults: DiskSearchResult {
    return Disk.search(forFile: offlineArtworkFileName)
  }

//  var isSongSaved: Bool {
//    return songDiskResults.found
//  }
//
//  var songFolder: Folder {
//    return songDiskResults.folder
//  }
//
//  var artworkFolder: Folder {
//    return artworkDiskResults.folder
//  }

//  var isArtworkSaved: Bool {
//    return artworkDiskResults.found
//  }
//
//  var songOfflineFilePath: String {
//    return songDiskResults.folder.relativeFilePath(withFileName: offlineFileName)
//  }
//
//  var artworkOfflineFilePath: String {
//    return artworkDiskResults.folder.relativeFilePath(withFileName: offlineFileName)
//  }
//
//  var isSongSavedOffline: Bool {
//    return songDiskResults.found && songDiskResults.folder.directory == Disk.Directory.documents
//  }
//
//  var isSongInTempDirectory: Bool {
//    return songDiskResults.found && songDiskResults.folder.directory == Disk.Directory.temporary
//  }
//
//  var isArtworkSavedOffline: Bool {
//    return artworkDiskResults.found && artworkDiskResults.folder.directory == Disk.Directory.caches
//  }
//
//  var isArtworkInTempDirectory: Bool {
//    return artworkDiskResults.found && artworkDiskResults.folder.directory == Disk.Directory.temporary
//  }

}
