//
//  AppDelegate+Testability.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 5/26/18.
//  Copyright © 2018 Mike Sabo. All rights reserved.
//

import Foundation
import UIKit
import ReSwift
import RealmSwift
import LifetimeTracker
import SwiftyJSON
import AppFolder
import SwiftyUserDefaults

var storedTracksTesting: [StoredTrack] = []

struct TestData {
  static var jsonTracks: JSON {
    guard let testData = R.file.library_test_dataJson.url(), let data = try? Data(contentsOf: testData) else {
      fatalError("Test Data was nnot found or currupted")
    }
    return JSON(data)
  }
}

extension AppDelegate {

  func setupDebuggingFeatures() {
    #if DEBUG
      setupTestData()
      startMemoryTracking()
    #endif
  }

  func setupTestData() {
    guard Defaults.string(forKey: "-unitTests") != nil else {
      return
    }

    TestData.jsonTracks["tracks"].arrayValue.forEach({ (trackJSON: JSON) in
      let newTrack = StoredTrack(json: trackJSON)
      storedTracksTesting.append(newTrack)
    })
  }

  func startMemoryTracking() {
    LifetimeTracker.setup(onUpdate: LifetimeTrackerDashboardIntegration(visibility: .alwaysVisible, style: .bar).refreshUI)
  }
}
