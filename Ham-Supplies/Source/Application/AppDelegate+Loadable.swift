//
//  AppDelegate+Loadable.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/22/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import SwiftyJSON
import Disk
import SwiftyUserDefaults

protocol AppDelegateLoadable: class {
  func setupRootViewController()
  func setupOfflineDirectories()
//  func reloadTracksInRealm()
  func migrateRealm(completion: @escaping () -> Void)
  func setupLogger()
  func getAllTracks()
}

extension AppDelegate: AppDelegateLoadable {

  func setupRootViewController() {
    //    let musicIcon = R.image.playing_tab_icon()
    let homeIcon = R.image.home_tab_icon()
    let playIcon = R.image.play_tab_icon()
    let libraryIcon = R.image.library_tab_icon()
    //    let mixedImage = UIImage(icon: FontType.icofont(.soundWave), size: CGSize(width: 40, height: 40))

    let mixedData = UINavigationController(rootViewController: MixedDataViewController())
    let player = PlayerViewController()
    let playlistVC = UINavigationController(rootViewController: PlaylistViewController())
    mixedData.tabBarItem = UITabBarItem(title: "", image: homeIcon, selectedImage: homeIcon)
    player.tabBarItem = UITabBarItem(title: "", image: playIcon, selectedImage: playIcon)
    playlistVC.tabBarItem = UITabBarItem(title: "", image: libraryIcon, selectedImage: libraryIcon)

    //    UICollectionView.appearance().backgroundColor = .white
    let tabBarController = UITabBarController()
    tabBarController.tabBar.tintColor = ColorPalatte.NeonBlue.peterRiver
    tabBarController.tabBar.unselectedItemTintColor = ColorPalatte.Grey.zinc
    tabBarController.tabBar.barTintColor = ColorPalatte.Blue.darkness
    let viewControllers: [UIViewController] = [mixedData, player, playlistVC]
    tabBarController.viewControllers = viewControllers
    player.preloadView()
    window?.rootViewController = tabBarController
  }

  func setupOfflineDirectories() {
    FileManager.default.createOfflineTrackCacheDirectories()
  }

//  func reloadTracksInRealm() {
//    DispatchQueue.global(qos: .default).async {
//
//      TrackCollection.shared.updateTracksModel(inRealm: DefaultRealmStore())
//    }
//  }

  func migrateRealm(completion: @escaping () -> Void) {
    #if TEST
      Realm.Configuration.defaultConfiguration.inMemoryIdentifier = "test"
      _ = try! Realm()
    #else
      Realm.Configuration.defaultConfiguration = Realm.Configuration(
        fileURL: DefaultRealmStore().fileURL,
        schemaVersion: 2,
        migrationBlock: { _, oldSchemaVersion in
          switch oldSchemaVersion {
          default:
            break
          }
      })
      _ = try! Realm()
    #endif
    completion()
  }

  func setupLogger() {
    //let console = ConsoleDestination()  // log to Xcode Console
    //let file = FileDestination()  // log to default swiftybeaver.log file
//    console.format = "$DHH:mm:ss$d $L $M"
//    console.format = "$DHH:mm:ss.SSS$d $C$L$c $T -> $N.$F:$l - $M"

    // log.addDestination(console)
    // log.addDestination(file)

  }

  func getAllTracks() {

    do {
      let realm = try Realm()
      guard realm.objects(StoredTrack.self).count == 0 else {
        return
      }
      print("Not loading File, Stored Tracks Exist")
      
//      let cacheProvider = (for: .cachesDirectory, in: .userDomainMask)
      let localLibraryURL = Router.latestLibraryLocalURL ?? URL(fileURLWithPath: "")
      let bundleLibraryURL = Bundle.main.url(forResource: "ham_supplies_latest", withExtension: "json") ?? URL(fileURLWithPath: "")
      var libraryURL: URL
      switch Disk.exists(localLibraryURL.path, in: .caches) {//.fileExists(atPath: localLibraryURL.path) {
      case true:
        libraryURL = localLibraryURL
      case false:
        libraryURL = bundleLibraryURL
      }

      let data = try Data(contentsOf: libraryURL)
      let json = JSON(data)
      realm.beginWrite()
      json["tracks"].arrayValue.forEach({ (trackJSON: JSON) in
        let newTrack = StoredTrack(json: trackJSON)
        realm.add(newTrack, update: true)
      })
      try realm.commitWrite()
      Defaults[.lastUpdatedLibrary] = json["version"].stringValue
    } catch {
      assertionFailure("Could not find ham songs JSON file")
    }

  }

}
