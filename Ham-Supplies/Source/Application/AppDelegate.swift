//  AppDelegate.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/1/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import UIKit
import ReSwift
import RealmSwift

import MediaPlayer
import SwiftyJSON
import AppFolder
import HockeySDK
import SwiftyUserDefaults
let store = Store<AppState>(reducer: appReducer, state: nil)

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    Defaults.registerUserDefaults()
    initializeAnalytics()
    migrateRealm(completion: { })
    getAllTracks()
    window = UIWindow(frame: UIScreen.main.bounds)
    setupOfflineDirectories()
    setupRootViewController()
    setupLogger()
    window?.makeKeyAndVisible()
    print(AppFolder.Documents.baseURL)
    setupDebuggingFeatures()
    return true
  }
  
  private func initializeAnalytics() {
    #if DEBUG
    BITHockeyManager.shared().testIdentifier()
    #else
    BITHockeyManager.shared().configure(withIdentifier: "38d5bc4ea4e7439a81811cb71911f7f8")
  // Do some additional configuration if needed here
    #endif
    BITHockeyManager.shared().isUpdateManagerDisabled = true
    BITHockeyManager.shared().isStoreUpdateManagerEnabled = false
    BITHockeyManager.shared().start()
    BITHockeyManager.shared().authenticator.authenticateInstallation()
  }
  
  func applicationWillResignActive(_ application: UIApplication) {

  }

  func applicationDidEnterBackground(_ application: UIApplication) {

  }

  func applicationWillEnterForeground(_ application: UIApplication) {

  }

  func applicationDidBecomeActive(_ application: UIApplication) {

  }

  func applicationWillTerminate(_ application: UIApplication) {

  }

}
