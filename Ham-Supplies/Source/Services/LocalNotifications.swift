//
//  LocalNotifications.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/29/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation
import NotificationCenter

enum NotificationKey: String {
  case trackSavedOffline

  var notificationName: NSNotification.Name {
    return Notification.Name(self.rawValue)
  }

  var notification: Notification {
    return Notification(name: self.notificationName)
  }
}

final class LocalNotification {

  init() { }

  static var shared = LocalNotification()

  let center = NotificationCenter.default

//  func post(key: NotificationKey) {
//    center.post(key.notification)
//  }

  func post(key: NotificationKey, withInfo info: [AnyHashable: Any]? = nil) {
    center.post(name: key.notificationName, object: nil, userInfo: info)
  }

}
