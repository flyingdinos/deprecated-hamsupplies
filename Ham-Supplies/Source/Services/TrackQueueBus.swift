//
//  TrackQueueBus.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/8/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation
import RxSwift

class TrackQueueBus {

  class var shared: TrackQueueBus {
    struct Singleton {
      static let instance = TrackQueueBus()
    }
    return Singleton.instance
  }
  var trackQueue: TrackQueue = TrackQueue<String>()
//  var trackQueue: [Track] = []
  var playingSubject: PublishSubject<String> = PublishSubject<String>()
  var trackImageSubject: PublishSubject<UIImage> = PublishSubject<UIImage>()
  var trackQueueSubject: PublishSubject<TrackQueue> = PublishSubject<TrackQueue<String>>()

  init() {

  }

  func playTrack(_ track: String) {
    trackQueue = TrackQueue(track: track)
    playingSubject.onNext(track)
  }

  func playPlaylist(_ tracks: [String]) {
    guard tracks.count > 0 else { return }
    trackQueue = TrackQueue(tracks: tracks)
    trackQueue.shuffle()
    playingSubject.onNext(tracks[0])
  }

  func addTrackToQueue(_ track: String) {
    trackQueue.enqueue(track)
  }

  func playTrackNext(_ track: String) {
    trackQueue.enqueueNext(element: track)
  }

  var peekNextTrackID: TrackID? {
    return trackQueue.peekNext()
  }

  var currentTrack: TrackID? {
    return trackQueue.current
  }

  func nextSong() {
    guard trackQueue.hasNext else { return }
    _ = trackQueue.dequeue()
    guard let nextTrack = trackQueue.current else {
      return
    }
    playingSubject.onNext(nextTrack)
  }

  private var queueRemainingCount: Int {
    return trackQueue.remaining
  }

  private var queuePlayedCount: Int {
    return trackQueue.played
  }

  var hasNext: Bool {
    return trackQueue.hasNext
  }

  var hasPrevious: Bool {
    return trackQueue.hasPrevious
  }

  func playPreviousSong() {
    guard hasPrevious else { return }
    guard let track = trackQueue.previousItem() else {
      return
    }
    playingSubject.onNext(track)
  }

  func playingObserver() -> Observable<String> {
    return playingSubject.asObservable()
  }

  func publish(trackImage image: UIImage) {
    trackImageSubject.onNext(image)
  }

  func trackArtworkObserver() -> Observable<UIImage> {
    return trackImageSubject.asObservable()
  }

}
