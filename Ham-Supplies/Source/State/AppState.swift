//
//  AppState.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/16/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation
import ReSwift

struct AppState: StateType {
  var trackQueue: TrackQueueState
  var audioPlayback: AudioPlaybackState

}
