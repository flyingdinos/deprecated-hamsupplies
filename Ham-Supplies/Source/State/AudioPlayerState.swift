//
//  AudioPlayerState.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/18/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation
import AVFoundation

struct AudioPlayerState {
  var assetPlayer: AVPlayer

}
