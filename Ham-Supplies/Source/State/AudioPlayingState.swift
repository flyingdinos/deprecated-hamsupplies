//
//  AudioPlayingState.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/17/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation

struct AudioPlaybackState {
  var isPlaying: Bool = false
  var currentTrack: StoredTrack?

}
