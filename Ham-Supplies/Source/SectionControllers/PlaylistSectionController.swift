//
//  PlaylistSectionController.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/25/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import UIKit
import IGListKit
import RealmSwift
import Realm
import CZPicker

final class PlaylistItem: NSObject {

  let playlists: [Playlist]?
  let itemCount: Int

  init(playlists: [Playlist]? = [], itemCount: Int = 0) {
    self.playlists = playlists
    self.itemCount = itemCount
  }

}

extension PlaylistItem: ListDiffable {

  func diffIdentifier() -> NSObjectProtocol {
    return self
  }

  func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
    return self === object ? true : self.isEqual(object)
  }

}

protocol PlaylistSelectedDelegate: class {
  func selectedPlaylist(withPrimaryKey: Int, atIndex index: Int)
}

final class PlaylistSectionController: ListSectionController {

  private var object: PlaylistItem?
  weak var selectedDelegate: PlaylistSelectedDelegate?
//  fileprivate var selectedTrackID: TrackID?

  override init() {
    super.init()
    self.inset = UIEdgeInsets(top: 3, left: 0, bottom: 0, right: 0)
    self.minimumInteritemSpacing = 1
    self.minimumLineSpacing = 1
  }

  override func numberOfItems() -> Int {
    return object?.itemCount ?? 0
  }

  override func didSelectItem(at index: Int) {

    guard index < numberOfItems(), let playlist = object?.playlists?[index] else {
      return
    }
    // log.debug("Selected \(playlist.name)")
//    let playlistRef = ThreadSafeReference(to: playlist)
//    selectedDelegate?.selectedPlaylist(playlistRef, atIndex: index)
    selectedDelegate?.selectedPlaylist(withPrimaryKey: playlist.id, atIndex: index)
  }

  override func sizeForItem(at index: Int) -> CGSize {
    let width = collectionContext?.containerSize.width ?? 0
    //    let itemSize = floor(width / 4)
    return CGSize(width: width, height: 50)
  }

  override func cellForItem(at index: Int) -> UICollectionViewCell {

    guard let cell = collectionContext?.dequeueReusableCell(withNibName: "PlaylistCollectionViewCell", bundle: nil, for: self, at: index) as? PlaylistCollectionViewCell else {
      fatalError()
    }
    if let playlist = object?.playlists?[index] {
      cell.index = index
      cell.playlist = playlist
    }
    return cell
  }

  override func didUpdate(to object: Any) {
    self.object = object as? PlaylistItem
  }

}
