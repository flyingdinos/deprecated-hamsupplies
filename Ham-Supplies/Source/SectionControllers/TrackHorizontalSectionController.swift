//
// TrackHorizontalSectionController.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/19/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import UIKit
import IGListKit

final class TrackHorizontalSectionController: ListSectionController, ListAdapterDataSource {

  var recentTracks: RecentTrackItem?

  override init() {
    super.init()
    self.minimumInteritemSpacing = 1
    self.minimumLineSpacing = 0
  }

  lazy var adapter: ListAdapter = {
    let adapter = ListAdapter(updater: ListAdapterUpdater(),
                              viewController: self.viewController)
    adapter.dataSource = self
    return adapter
  }()

  override func sizeForItem(at index: Int) -> CGSize {
    return CGSize(width: collectionContext!.containerSize.width, height: 125)
  }

  override func cellForItem(at index: Int) -> UICollectionViewCell {
    guard let cell = collectionContext?.dequeueReusableCell(of: EmbeddedCollectionViewCell.self,
                                                            for: self,
                                                            at: index) as? EmbeddedCollectionViewCell else {
                                                              fatalError()
    }
    adapter.collectionView = cell.collectionView
    return cell
  }

  override func didUpdate(to object: Any) {
    recentTracks = object as? RecentTrackItem
  }

  // MARK: ListAdapterDataSource

  func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
    guard let tracks = recentTracks else { return [] }
    return tracks.track.map({ $0 })
  }

  func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
    return TrackEmbeddedSectionController()
  }

  func emptyView(for listAdapter: ListAdapter) -> UIView? {
    return nil
  }

}
