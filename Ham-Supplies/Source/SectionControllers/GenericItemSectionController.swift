//
//  GenericItemSectionController.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 11/1/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import UIKit
import IGListKit
import RealmSwift
import Realm
import CZPicker

class GenericItem {
  var title: String
  var itemCount: Int
  
  init(title: String, count: Int) {
    self.title = title
    self.itemCount = count
  }
}

class GenericCollection: NSObject {
  
  var items: [GenericItem]
  var count: Int
  
  init(items: [GenericItem], count: Int) {
    self.items = items
    self.count = count
  }
}

extension GenericCollection: ListDiffable {
  
  func diffIdentifier() -> NSObjectProtocol {
    return self
  }
  
  // 2
  func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
    return self === object ? true : self.isEqual(object)
  }
}

extension GenericItem: ListDiffable {
  
  func diffIdentifier() -> NSObjectProtocol {
    return title as NSObjectProtocol
  }
  
  // 2
  func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
    return true
  }
}

protocol GenericItemSelectedDelegate: class {
  func selectedItem(withKey: String, atIndex index: Int)
}

final class GenericItemSectionController: ListSectionController {
  
  private var object: GenericCollection?
  weak var selectedDelegate: GenericItemSelectedDelegate?
  //  fileprivate var selectedTrackID: TrackID?
  
  override init() {
    super.init()
    self.inset = UIEdgeInsets(top: 3, left: 0, bottom: 0, right: 0)
    self.minimumInteritemSpacing = 1
    self.minimumLineSpacing = 1
  }
  
  override func numberOfItems() -> Int {
    return object?.items.count ?? 0
  }
  
  override func didSelectItem(at index: Int) {
    
    guard index < numberOfItems(), let item = object?.items[index] else {
      return
    }
    selectedDelegate?.selectedItem(withKey: item.title, atIndex: index)
  }
  
  override func sizeForItem(at index: Int) -> CGSize {
    let width = collectionContext?.containerSize.width ?? 0
    //    let itemSize = floor(width / 4)
    return CGSize(width: width, height: 50)
  }
  
  override func cellForItem(at index: Int) -> UICollectionViewCell {
    
    guard let cell = collectionContext?.dequeueReusableCell(withNibName: "GenericTrackFilterCell", bundle: nil, for: self, at: index) as? GenericTrackFilterCell else {
      fatalError()
    }
    if let item = object?.items[index] {
      cell.index = index
      cell.title = item.title
      cell.songCount = item.itemCount
    }
    return cell
  }
  
  override func didUpdate(to object: Any) {
    self.object = object as? GenericCollection
  }
  
}
