//
//  TrackItemSectionController.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/19/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import UIKit
import IGListKit
import RealmSwift
import CZPicker
import Realm

final class TrackItem: NSObject {

  let tracks: [Track]
  let itemCount: Int
  let sectionHeader: String

  init(trackArray: [Track] = [], andSectionHeader header: String) {
    self.tracks = trackArray
    self.itemCount = self.tracks.count
    self.sectionHeader = header
  }

}

//final class TrackItemSectionController: ListSectionController {
//
//  private var object: TrackItem?
//  fileprivate var playlistResults: Results<Playlist>?
//  fileprivate var selectedTrackID: TrackID?
//
//  override init() {
//    super.init()
//    self.minimumInteritemSpacing = 1
//    self.minimumLineSpacing = 0
//    self.supplementaryViewSource = self
//  }
//
//  override func numberOfItems() -> Int {
//    return object?.itemCount ?? 0
//  }
//
//    override func didSelectItem(at index: Int) {
//
//      if let currentTrack = TrackQueueBus.shared.trackQueue.current,
//        let newTrack = object?.tracks[index], AudioPlayer.instance.isPlaying {
//
//        let alert = UIAlertController(title: "Currently Playing",
//                                      message: "Do you want to play this song now, or queue next",
//                                      preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
//        alert.addAction(UIAlertAction(title: "Play Now", style: .default, handler: { (_) in
//          //        store.dispatch(TrackQueuePlayNow(track: newTrack))
//          TrackQueueBus.shared.playTrack(newTrack.id)
//        }))
//        alert.addAction(UIAlertAction(title: "Play Next", style: .default, handler: { (_) in
//          //        store.dispatch(TrackQueueEnqueNext(track: newTrack))
//          TrackQueueBus.shared.playTrackNext(newTrack.id)
//        }))
//        alert.addAction(UIAlertAction(title: "Add to Queue", style: .default, handler: { (_) in
//          //        store.dispatch(TrackQueueAddToQueue(track: newTrack))
//          TrackQueueBus.shared.addTrackToQueue(newTrack.id)
//        }))
//        self.viewController?.present(alert, animated: true, completion: nil)
//      } else {
//        if let track = object?.tracks[index] {
//          //        store.dispatch(TrackQueuePlayNow(track: track))
//          TrackQueueBus.shared.playTrack(track.id)
//          //        AudioPlayer.instance.selected(track: track)
//        }
//      }
//    }
//
//    override func sizeForItem(at index: Int) -> CGSize {
//      let width = collectionContext?.containerSize.width ?? 0
//      //    let itemSize = floor(width / 4)
//      return CGSize(width: width, height: 50)
//    }
//
//    override func cellForItem(at index: Int) -> UICollectionViewCell {
//
//      guard let cell = collectionContext?.dequeueReusableCell(withNibName: "IndividualTrackCollectionViewCell", bundle: nil, for: self, at: index) as? IndividualTrackCollectionViewCell else {
//        fatalError()
//      }
//      if let track = object?.tracks[index] {
//        cell.delegate = self
//        let viewModel = IndividualTrackViewModel(track: track, itemIndex: index)
//        cell.cellViewModel = viewModel
//      }
//      return cell
//    }
//
//  override func didUpdate(to object: Any) {
//    self.object = object as? TrackItem
//  }
//
//}
//
//extension TrackItemSectionController: ListSupplementaryViewSource {
//
//  func supportedElementKinds() -> [String] {
//    return [UICollectionElementKindSectionHeader]
//  }
//
//  func viewForSupplementaryElement(ofKind elementKind: String, at index: Int) -> UICollectionReusableView {
//
//    guard let view = collectionContext?
//      .dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader,
//                                        for: self,
//                                        class: LabelCell.self,
//                                        at: index) as? LabelCell else {
//       fatalError("Could not create Supplementary View")
//    }
//    view.textColor = ColorPalatte.Grey.zinc
//    view.contentViewColor = ColorPalatte.Blue.midnightBlue
//    view.text = object?.sectionHeader
//    return view
//  }
//
//  func sizeForSupplementaryView(ofKind elementKind: String, at index: Int) -> CGSize {
//    guard let header = object?.sectionHeader, header.count > 0 else { return CGSize(width: collectionContext!.containerSize.width, height: 0)
//    }
//    return CGSize(width: collectionContext!.containerSize.width, height: 40)
//  }
//
//}
//
//extension TrackItemSectionController: TrackOptionsDelegate {
//
//  func touchedMoreOptions(forIndex index: Int, withTrackID trackID: TrackID) {
//    print("Touched options for \(trackID) at index \(index)")
//    selectedTrackID = trackID
//    let alert = UIAlertController(title: "More Options",
//                                  message: "",
//                                  preferredStyle: .alert)
//    alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: { _ in
//      self.selectedTrackID = nil
//    }))
//
//    alert.addAction(UIAlertAction(title: "Play Now", style: .default, handler: { (_) in
//      TrackQueueBus.shared.playTrack(trackID)
//      self.selectedTrackID = nil
//    }))
//
//    alert.addAction(UIAlertAction(title: "Add to Playlist", style: .default, handler: { _ in
//      DispatchQueue.main.async {
//        self.showPlaylist()
//      }
//    }))
//
//    if AudioPlayer.instance.isPlaying {
//      alert.addAction(UIAlertAction(title: "Add to Queue", style: .default, handler: { (_) in
//        TrackQueueBus.shared.addTrackToQueue(trackID)
//        self.selectedTrackID = nil
//      }))
//    }
//
//    self.viewController?.present(alert, animated: true, completion: nil)
//  }
//
//  var playlistCount: Int {
//    return playlistResults?.count ?? 0
//  }
//
//  func showPlaylist() {
//    let realm = try! Realm()
//    playlistResults = realm.objects(Playlist.self)
//
//    let picker = CZPickerView(headerTitle: "Add to Playlist's", cancelButtonTitle: "Cancel", confirmButtonTitle: "Confirm")
//    picker?.delegate = self
//    picker?.dataSource = self
//    picker?.needFooterView = false
//    picker?.allowMultipleSelection = true
//    picker?.tapBackgroundToDismiss = false
//    picker?.checkmarkColor = ColorPalatte.Blue.moonlight
//    //    picker?.cellBackgroundColor = ColorPalatte.Blue.midnightBlue
//
//    picker?.headerBackgroundColor = ColorPalatte.Blue.wetAsphalt
//    //    picker?.cancelButtonBackgroundColor = ColorPalatte.Blue.midnightBlue
//    picker?.confirmButtonBackgroundColor = ColorPalatte.Blue.wetAsphalt
//
//    picker?.headerTitleColor  = ColorPalatte.Grey.clouds
//    picker?.cancelButtonNormalColor = ColorPalatte.Blue.twilight
//    picker?.confirmButtonNormalColor = ColorPalatte.Grey.clouds
//    picker?.show()
//  }
//
//  func add(trackID: TrackID, toPlaylists selectedIndexes: [Int]) {
//    let realm = try! Realm()
//    guard let track = realm.object(ofType: StoredTrack.self, forPrimaryKey: trackID) else {
//      // log.debug("Could not retrieve track with id \(trackID)")
//      return
//    }
//    for index in selectedIndexes {
//      if index < playlistCount, let playlist = playlistResults?[index] {
//        playlist.addTrack(track, realm)
//      }
//    }
//  }
//}
//
//extension TrackItemSectionController: CZPickerViewDelegate, CZPickerViewDataSource {
//
//  func czpickerView(_ pickerView: CZPickerView!, imageForRow row: Int) -> UIImage! {
//    return nil
//  }
//
//  func numberOfRows(in pickerView: CZPickerView!) -> Int {
//    return playlistCount
//  }
//
//  func czpickerView(_ pickerView: CZPickerView!, attributedTitleForRow row: Int) -> NSAttributedString! {
//    if row < playlistCount, let title = playlistResults?[row].name {
//      let attributedString = NSAttributedString(string: title, attributes: [NSAttributedStringKey.foregroundColor: ColorPalatte.Blue.moonlight])
//      return attributedString
//    }
//    return NSAttributedString(string: "")
//  }
//
//  func czpickerViewDidClickCancelButton(_ pickerView: CZPickerView!) {
//
//  }
//
//  func czpickerView(_ pickerView: CZPickerView!, didConfirmWithItemsAtRows rows: [Any]!) {
//    guard let selectedRows = rows as? [Int],
//      let trackID = selectedTrackID else {
//        return
//    }
//    add(trackID: trackID, toPlaylists: selectedRows)
//  }
//}
//
////extension TrackItemSectionController: ListBindingSectionControllerDataSource {
////
////  func sectionController(_ sectionController: ListBindingSectionController<ListDiffable>, viewModelsFor object: Any) -> [ListDiffable] {
////    guard let trackItem = object as? TrackItem else { return [] }
////    var viewModels = [ListDiffable]()
////
////    for (index, item) in trackItem.track.enumerated() {
////
////      viewModels.append(viewModel)
////    }
////
////    return viewModels
////  }
////
////  func sectionController(_ sectionController: ListBindingSectionController<ListDiffable>, cellForViewModel viewModel: Any, at index: Int) -> UICollectionViewCell & ListBindable {
////    let cellClass = IndividualTrackCollectionViewCell.self
////
////    guard let cell = collectionContext?.dequeueReusableCell(of: cellClass, for: self, at: index) as? IndividualTrackCollectionViewCell else {
////      guard let otherCell = collectionContext?.dequeueReusableCell(of: cellClass, for: self, at: index) as? UICollectionViewCell & ListBindable else {
////        fatalError("Could not create a Individual track cell")
////      }
////      return otherCell
////    }
////    cell.delegate = self
////    return cell
////  }
////
////  func sectionController(_ sectionController: ListBindingSectionController<ListDiffable>,
////                         sizeForViewModel viewModel: Any,
////                         at index: Int) -> CGSize {
////    let width = collectionContext?.containerSize.width ?? 0
////    return CGSize(width: width, height: 50)
////  }
////
////  // MARK: ListBindingSectionControllerSelectionDelegate
////
////  func sectionController(_ sectionController: ListBindingSectionController<ListDiffable>, didSelectItemAt index: Int, viewModel: Any) {
////
////    self.sectionController(sectionController, didDeselectItemAt: index, viewModel: viewModel)
////
////    guard let cellViewModel = viewModel as? IndividualTrackViewModel else { return }
////    let newTrack = cellViewModel.track
////    if AudioPlayer.instance.isPlaying {
////      let alert = UIAlertController(title: "Currently Playing",
////                                    message: "Do you want to play this immediately, next, or later?",
////                                    preferredStyle: .alert)
////      alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
////      alert.addAction(UIAlertAction(title: "Play Now", style: .default, handler: { (_) in
////        TrackQueueBus.shared.playTrack(newTrack.id)
////      }))
////      alert.addAction(UIAlertAction(title: "Play Next", style: .default, handler: { (_) in
////        TrackQueueBus.shared.playTrackNext(newTrack.id)
////      }))
////      alert.addAction(UIAlertAction(title: "Add to Queue", style: .default, handler: { (_) in
////        TrackQueueBus.shared.addTrackToQueue(newTrack.id)
////      }))
////      self.viewController?.present(alert, animated: true, completion: nil)
////    } else {
////      TrackQueueBus.shared.playTrack(newTrack.id)
////    }
////
////  }
////
////  func sectionController(_ sectionController: ListBindingSectionController<ListDiffable>, didDeselectItemAt index: Int, viewModel: Any) {
////
////  }
////
////}
