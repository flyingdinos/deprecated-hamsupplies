//
//  TrackEmbeddedSectionController.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/19/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import UIKit
import IGListKit

final class TrackEmbeddedSectionController: ListSectionController {

  var track: Track?

  override init() {
    super.init()
    self.inset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 2)
//    self.minimumInteritemSpacing = 3
    self.minimumLineSpacing = 0
  }

  override func sizeForItem(at index: Int) -> CGSize {

    return CGSize(width: 120, height: 120)
  }

  override func cellForItem(at index: Int) -> UICollectionViewCell {

    guard let cell = collectionContext?.dequeueReusableCell(withNibName: "TrackCollectionViewCell", bundle: nil, for: self, at: index) as? TrackCollectionViewCell else {
      fatalError()
    }
    cell.artist = track?.artist
    cell.title = track?.title
    return cell
  }

  override func didUpdate(to object: Any) {
    track = object as? Track
  }

}
