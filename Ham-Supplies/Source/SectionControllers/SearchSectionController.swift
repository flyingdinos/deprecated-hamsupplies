//
//  SearchViewController.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/19/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation
import UIKit
import IGListKit

protocol SearchSectionControllerDelegate: class {
  func searchSectionController(_ sectionController: SearchSectionController, didChangeText text: String)
}

final class SearchSectionController: ListSectionController, UISearchBarDelegate, ListScrollDelegate {

  weak var delegate: SearchSectionControllerDelegate?

  override init() {
    super.init()
    scrollDelegate = self
  }

  convenience init(withSearchText text: String? = nil) {
    self.init()
    searchText = text
  }
  
  var searchText: String?
  
  override func sizeForItem(at index: Int) -> CGSize {
    return CGSize(width: collectionContext!.containerSize.width, height: 44)
  }

  override func cellForItem(at index: Int) -> UICollectionViewCell {
    guard let cell = collectionContext?.dequeueReusableCell(withNibName: TrackSearchBarCollectionViewCell.nibName, bundle: nil, for: self, at: index) as? TrackSearchBarCollectionViewCell else {
      fatalError()
    }
    cell.searchBar.delegate = self
    cell.setColors()
    if let text = searchText {
      cell.searchBar?.text = text
    }
    return cell
  }
  
  func setSearchBar(withText text: String) {
    
  }

  // MARK: UISearchBarDelegate

  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    guard searchText.count > 1 else { return }
    delegate?.searchSectionController(self, didChangeText: searchText)
  }

  func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//    delegate?.searchSectionController(self, didChangeText: "")
    searchBar.resignFirstResponder()
  }

  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    searchBar.resignFirstResponder()
  }

  // MARK: ListScrollDelegate

  func listAdapter(_ listAdapter: ListAdapter, didScroll sectionController: ListSectionController) {
    if let searchBar = (collectionContext?.cellForItem(at: 0, sectionController: self) as? TrackSearchBarCollectionViewCell)?.searchBar {
      searchBar.resignFirstResponder()
    }
  }

  func listAdapter(_ listAdapter: ListAdapter, willBeginDragging sectionController: ListSectionController) {}
  func listAdapter(_ listAdapter: ListAdapter,
                   didEndDragging sectionController: ListSectionController,
                   willDecelerate decelerate: Bool) {}

}
