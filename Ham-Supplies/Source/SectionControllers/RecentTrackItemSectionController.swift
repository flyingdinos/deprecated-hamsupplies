//
//  TrackItemSectionController.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/27/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation
import IGListKit

final class RecentTrackItem: NSObject {

  let track: [StoredTrack]
  let itemCount: Int

  init(track: [StoredTrack], itemCount: Int) {
    self.track = track
    self.itemCount = itemCount
  }

}

extension RecentTrackItem: ListDiffable {

  func diffIdentifier() -> NSObjectProtocol {
    return self
  }

  func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
    return self === object ? true : self.isEqual(object)
  }

}
