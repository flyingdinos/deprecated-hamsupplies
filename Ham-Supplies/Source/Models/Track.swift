//
//  Track.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/1/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation
import IGListKit
import SwiftyJSON
import Disk
import Realm
import RealmSwift

class Track: TrackDataModel {

  var id: String = ""
  var time: Int = -1
  var year: Int = -1
  var bpm: Int = -1
  var key: String = ""
  var energy: String = ""
  var bitrate: Int = -1
  var artist: String = ""
  var title: String = ""
  var album: String = ""
  var label: String = ""
  var genre: String = ""
  var released: String = ""
  var added: String = ""
  var file: String = ""
  var artwork: String = ""

  var dateAdded: Date?
  var dateReleased: Date?

  init() {

  }

  init(json: JSON) {
    self.id = json["id"].stringValue
    self.time = json["time"].intValue
    self.year = json["year"].intValue
    self.bpm = json["bpm"].intValue
    self.key = json["key"].stringValue
    self.energy = json["energy"].stringValue
    self.bitrate = json["bitrate"].intValue
    self.artist = json["artist"].stringValue
    self.title = json["title"].stringValue
    self.album = json["album"].stringValue
    self.label = json["label"].stringValue
    self.genre = json["genre"].stringValue
    self.released = json["released"].stringValue
    self.added = json["added"].stringValue
    self.file = json["file"].stringValue
    self.artwork = json["artwork"].stringValue
    self.dateAdded = utcDateFormatter.date(from: self.added)
    self.dateReleased = utcDateFormatter.date(from: self.released)
  }
}

extension Track: Equatable {

  public static func ==(lhs: Track, rhs: Track) -> Bool {
    return lhs.id == rhs.id
  }
}

extension Track: Hashable {

  var hashValue: Int {
    return id.hashValue << 3
  }

}

extension Track: ListDiffable {
  func diffIdentifier() -> NSObjectProtocol {
    return id as NSObjectProtocol
  }

  // 2
  func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
    return true
  }
}

extension Array where Element: Track {

  func mostRecentAdded() -> [Track] {
    guard self.count > 0 else { return [] }
    let sorted = self.sorted(by: { one, two in
      guard let dateOne = one.dateAdded, let dateTwo = two.dateAdded else {
        return false
      }
      return dateOne > dateTwo
    })

    let mostRecent = sorted[0..<12]
    return Array(mostRecent)
  }

  func tracks(forYear year: Int) -> [Track] {
    guard self.count > 0 else { return [] }
    return self.filter({ $0.year == year })
      .sorted(by: { one, two in
        return one.artist > two.artist
    })
  }

}

var utcDateFormatter: DateFormatter {
  let formatter = DateFormatter()
  formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
  return formatter
}
