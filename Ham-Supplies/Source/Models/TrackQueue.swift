//
//  TrackQueue.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/8/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation

struct TrackQueue<T> {

  fileprivate var array = [T?]()
  fileprivate var head = 0

  public var isEmpty: Bool {
    return array.count == 0
  }

  public init() {
    array = []
    head = 0
  }

  public init(track: T) {
    array = [track]
    head = 0
  }

  public init(tracks: [T]) {
    array = tracks
    head = 0
  }

  public mutating func playSingle(track: T) {
    array = [track]
    head = 0
  }

  public var remaining: Int {
    return array.count - (head + 1)
  }

  public mutating func enqueue(_ element: T) {
    array.append(element)
  }

  @discardableResult
  public mutating func dequeue() -> T? {
    guard hasNext, let element = array[head] else { return nil }
    head += 1
    return element
  }

  public mutating func enqueueNext(element: T) {
    let nextHead = head + 1
    array.insert(element, at: nextHead)
  }

  @discardableResult
  public mutating func previousItem() -> T? {
    guard head > 0 else { return nil }
    head -= 1
    return array[head]
  }

  public var hasNext: Bool {
    return (head + 1) < array.count && remaining > 0
  }

  public var count: Int {
    return array.count
  }

  public var played: Int {
    return head
  }

  public var hasPrevious: Bool {
    return (head > 0)
  }

  public func peekNext() -> T? {
    guard hasNext else { return nil }
    return array[head + 1]
  }

  public var current: T? {
    if head < array.count {
      return array[head]
    } else {
      print("no current")
      return nil
    }
  }

  public var front: T? {
    if isEmpty {
      return nil
    } else {
      return array[head]
    }
  }

  public var first: T? {
    if isEmpty {
      return nil
    } else {
      return array[0]
    }
  }

  public var elements: [T?] {
    return array
  }

  public mutating func shuffle() {
    let currentItem = current
    var remainingArray = Array(array[1..<array.count])

    for i in 0 ..< (remainingArray.count - 1) {
      let j = Int(arc4random_uniform(UInt32(remainingArray.count - i))) + i
      remainingArray.swapAt(i, j)
    }

    remainingArray.insert(currentItem, at: 0)
    array = remainingArray
    head = 0
  }
}
