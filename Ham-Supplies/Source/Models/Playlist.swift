//
//  Playlist.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/22/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
import IGListKit

class Playlist: Object {

  @objc dynamic var id: Int = -1
  @objc dynamic var name: String = ""
  @objc dynamic var saveOfflne: Bool = false
  @objc dynamic var dateCreated: Date = Date()

  let tracks = List<StoredTrack>()

  func incrementID( _ realm: Realm = try! Realm()) -> Int {
    return (realm.objects(Playlist.self).max(ofProperty: "id") as Int? ?? 0) + 1
  }

  func save(inRealm realm: Realm) {
    do {
      try realm.write {
        if (self.id < 0) {
          self.id = incrementID(realm)
        }
        realm.add(self, update: true)
      }
    } catch {
      print("Could not save playlist")
    }
  }
  
//  func saveTracksForOffline(_ shouldSaveTracks: Bool = true) {
//    do {
//      try realm.write {
//        if self.saveOffline && !shouldSaveTracks {
//          // Cleanup Saved Tracks
//        }
//
//        self.saveOfflne = shouldSaveTracks
//
//        realm.add(self, update: true)
//      }
//    } catch {
//      print("Could not save playlist")
//    }
//  }

  func addTrack(_ track: StoredTrack,
                _ realm: Realm = try! Realm()) {
    do {
      if (self.tracks.index(of: track) != nil) {
        return
      }
      try realm.write {
        self.tracks.append(track)
        realm.add(self, update: true)
      }
    } catch { }
  }

  @discardableResult
  func removeTrack(atIndex index: Int, inRealm realm: Realm = try! Realm()) -> Bool {
    guard tracks.count > 0, index < tracks.count else { return false }
    do {
      try realm.write {
        self.tracks.remove(at: index)
        realm.add(self, update: true)
      }
      return true
    } catch {
      print("Error deleting track at index \(index)")
    }
    return false
  }

  @discardableResult
  func removeTrack(atIndex index: Int, inRealm realm: Realm = try! Realm(), withoutNotifying notificationToken: NotificationToken?) -> Bool {
    guard tracks.count > 0, index < tracks.count,
    let token = notificationToken else { return false }
    do {
      realm.beginWrite()
      self.tracks.remove(at: index)
      try realm.commitWrite(withoutNotifying: [token])
      return true
    } catch {
      print("Error deleting track at index \(index)")
    }
    return false
  }

  override static func primaryKey() -> String? {
    return "id"
  }

}

extension Playlist: ListDiffable {
  func diffIdentifier() -> NSObjectProtocol {
    return id as NSObjectProtocol
  }

  // 2
  func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
    return true
  }
}
