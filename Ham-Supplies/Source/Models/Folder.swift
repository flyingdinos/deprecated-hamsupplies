//
//  Folder.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/13/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation
import Disk
import AppFolder

let documents = AppFolder.Documents
let caches = AppFolder.Library.Caches
let cachesURL = caches.url

extension Documents {
  
  final class SavedFiles: Directory {
    final class Tracks: Directory { }
    
    var Tracks: Tracks {
      return subdirectory()
    }
    
    final class Artwork: Directory { }
    
    var Artwork: Artwork {
      return subdirectory()
    }
  }
  
  var SavedFiles: SavedFiles {
    return subdirectory()
  }
  
}

enum Folder {

  case savedTracks
  case savedArtwork
  case temporary
  case none

  var path: String {
    switch self {
    case .savedTracks:
      return "SavedFiles/Tracks/"
    case .savedArtwork:
      return "SavedFiles/Artwork/"
    default:
      return ""
    }
  }

  var directory: Disk.Directory {
    switch self {
    case .savedTracks, .savedArtwork:
      return .documents
    default:
      return .temporary
    }
  }

  func relativeFilePath(withFileName fileName: String) -> String {
    return path + fileName
  }

  func fileURL(withFileName fileName: String) -> URL {
    let filePath = relativeFilePath(withFileName: fileName)
    do {
      let fileURL = try Disk.getURL(for: filePath, in: directory)
      return fileURL
    } catch {
      print("Could not get file url")
    }
    return URL(fileURLWithPath: NSTemporaryDirectory())
  }

  func exists(withFileName fileName: String) -> Bool {
    let relativePath = relativeFilePath(withFileName: fileName)
    switch self {
    case .savedTracks:
      return Disk.exists(relativePath, in: .documents)
    case .savedArtwork:
      return Disk.exists(relativePath, in: .documents)
    case .temporary:
      return Disk.exists(relativePath, in: .temporary)
    default:
      return false
    }
  }

  static func exists(withID trackID: TrackID, inFolder folder: Folder) -> FolderSearchResults {
    var isFound = false
    var foundFileURL: URL?
    let cacheURL = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
    let savedTrackDirectory = cacheURL?.appendingPathComponent(Folder.savedTracks.path) ?? URL(fileURLWithPath: "")

    if let pathContents = try? FileManager.default.contentsOfDirectory(at: savedTrackDirectory, includingPropertiesForKeys: nil, options: .skipsHiddenFiles) {
      pathContents.forEach { file in
        let res = file.deletingPathExtension()
        if res.absoluteString.contains(trackID) {
          isFound = true
          foundFileURL = file
          return
        }
      }
    }
    return FolderSearchResults(found: isFound, fileURL: foundFileURL)
  }

  static func creatFileURL(forFile file: String, inFolder folder: Folder) -> URL {
    let relativePath = folder.relativeFilePath(withFileName: file)
    do {
      let savedTracksURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
      return savedTracksURL.appendingPathComponent(relativePath)
    } catch {
      print("Error creating url or saved track directory")
    }

    return URL(fileURLWithPath: relativePath)
  }

}

typealias FolderSearchResults = (found: Bool, fileURL: URL?)
