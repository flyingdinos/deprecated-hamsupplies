//
//  StoredTrack.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/22/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
import SwiftyJSON
import IGListKit
import Disk
import SwiftyUserDefaults

class StoredTrack: Object, TrackDataModel {

  @objc dynamic var id: String = ""
  @objc dynamic var time: Int = 0
  @objc dynamic var year: Int = 0
  @objc dynamic var bpm: Int = 0
  @objc dynamic var key: String = ""
  @objc dynamic var energy: String = ""
  @objc dynamic var bitrate: Int = 0
  @objc dynamic var artist: String = ""
  @objc dynamic var title: String = ""
  @objc dynamic var album: String = ""
  @objc dynamic var label: String = ""
  @objc dynamic var genre: String = ""
  @objc dynamic var released: String = ""
  @objc dynamic var added: String = ""
  @objc dynamic var file: String = ""
  @objc dynamic var artwork: String = ""

  @objc dynamic var playCount: Int = 0

  @objc dynamic var lastPlayed: Date?
  @objc dynamic var dateAdded: Date?
  @objc dynamic var dateReleased: Date?

  @objc dynamic var searchString: String = ""

  convenience init(track: Track) {
    self.init()
    self.id = track.id
    self.time = track.time
    self.year = track.year
    self.bpm = track.bpm
    self.key = track.key
    self.energy = track.energy
    self.bitrate = track.bitrate
    self.artist = track.artist
    self.title = track.title
    self.album = track.album
    self.label = track.label
    self.genre = track.genre
    self.released = track.released
    self.added = track.added
    self.file = track.file
    self.artwork = track.artwork
    self.dateAdded = track.dateAdded
    self.dateReleased = track.dateReleased
    refreshSearchString()
  }

  convenience init(json: JSON) {
    self.init()
    self.id = json["id"].stringValue
    self.time = json["time"].intValue
    self.year = json["year"].intValue
    self.bpm = json["bpm"].intValue
    self.key = json["key"].stringValue
    self.energy = json["energy"].stringValue
    self.bitrate = json["bitrate"].intValue
    self.artist = json["artist"].stringValue
    self.title = json["title"].stringValue
    self.album = json["album"].stringValue
    self.label = json["label"].stringValue
    self.genre = json["genre"].stringValue
    self.released = json["released"].stringValue
    self.added = json["added"].stringValue
    self.file = json["file"].stringValue
    self.artwork = json["artwork"].stringValue
    self.dateAdded = utcDateFormatter.date(from: self.added)
    self.dateReleased = utcDateFormatter.date(from: self.released)
    refreshSearchString()
  }

  override static func primaryKey() -> String? {
    return "id"
  }

  required init() {
    super.init()
  }

  required init(realm: RLMRealm, schema: RLMObjectSchema) {
    super.init(realm: realm, schema: schema)
  }

  required init(value: Any, schema: RLMSchema) {
    super.init(value: value, schema: schema)
  }

  override var hashValue: Int {
    return id.hashValue << 3
  }

  func refreshSearchString() {
    self.searchString = "\(title) \(artist) \(album) \(genre) \(year)".lowercased()
  }

  static func updatePlayCount(forTrackID trackID: TrackID) {
    do {
      let realm = try Realm()
      guard let track = realm.object(ofType: StoredTrack.self, forPrimaryKey: trackID) else {
        return
      }
      let currentTime = Date()
      let previousTime = track.lastPlayed ?? currentTime
      if currentTime == previousTime || abs(previousTime.timeIntervalSinceNow) > TimeInterval(track.time) {
        print("Updating playCount for trackID: \(track.id)")
        realm.beginWrite()
        track.playCount += 1
        track.lastPlayed = currentTime
        try realm.commitWrite()
      }
    } catch {
      print("Could not update play count")
    }
  }

  static func updateLibrary(withLibraryURL libraryURL: URL) {
    guard let data = try? Data(contentsOf: libraryURL) else { return }
    let json = JSON(data)
    let tracks = json["tracks"].arrayValue.map({ (trackJSON: JSON) -> Track in
      return Track(json: trackJSON)
    })
//    TrackCollection.shared = TrackCollection(tracks: tracks)
    DispatchQueue.global(qos: .default).async {
      autoreleasepool {
        do {
          let realm = try Realm()
          let storedTracks = tracks.map({ obj in
            return realm.object(ofType: StoredTrack.self, forPrimaryKey: obj.id) ?? StoredTrack(track: obj)
          })
          realm.beginWrite()
          storedTracks.forEach({ element in
            element.refreshSearchString()
            realm.add(element, update: true)
          })
          try realm.commitWrite()
        } catch let error as NSError {
          print("Error writing all tracks to memory \(error.localizedDescription)")
        }
      }
      Defaults[.lastUpdatedLibrary] = json["version"].stringValue
    }
  }

  static func allTracks(inRealm realm: Realm = try! Realm()) -> Results<StoredTrack> {
    let tracks = realm.objects(StoredTrack.self).sorted(byKeyPath: "title", ascending: true)
    print(tracks.count)
    return tracks
  }
}

extension StoredTrack: ListDiffable {
  func diffIdentifier() -> NSObjectProtocol {
    return id as NSObjectProtocol
  }

  // 2
  func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
    return true
  }
}

extension StoredTrack: TrackSearchable {

}

extension Array where Element: StoredTrack {

  func mostRecentAdded() -> [StoredTrack] {
    let sorted = self.sorted(by: { one, two in
      guard let dateOne = one.dateAdded, let dateTwo = two.dateAdded else {
        return false
      }
      return dateOne > dateTwo
    })

    let mostRecent = sorted[0..<12]
    return Array(mostRecent)
  }

  func tracks(forYear year: Int) -> [StoredTrack] {
    return self.filter({ $0.year == year })
      .sorted(by: { one, two in
        return one.artist > two.artist
      })
  }

}
