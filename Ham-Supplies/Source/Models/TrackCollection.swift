//
//  TrackCollection.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/24/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation
import RealmSwift

//struct TrackCollection {
//
//  private var array: [Track] = []
//  static var shared = TrackCollection()
//
//  init() { }
//
//  init(tracks: [Track]) {
//    array = tracks
//  }
//
//  var elements: [Track] {
//    return array
//  }
//
//  func artistSort() -> [Track] {
//    return array.sorted(by: { $0.artist < $1.artist })
//  }
//
//  func trackSort() -> [Track] {
//    return array.sorted(by: { $0.title < $1.title })
//  }
//
//  func mapTitlesAsTrackItem() -> [TrackItem] {
//    let sections = array.map({ $0.title.characters.first }).flatMap({ $0 }).sorted()
//    let trackItems = sections.map { char -> TrackItem in
//      let tracks = array.filter({ track in
//        let titleChar = track.title.characters.first
//        return char == titleChar
//      })
//      return TrackItem(trackArray: tracks, andSectionHeader: String(char).uppercased())
//    }
//    return trackItems //array.sorted(by: { $0.artist < $1.artist })
//  }
//
//  func mapArtistAsTrackItem() -> [TrackItem] {
//    let sections = array.map({ $0.artist.characters.first }).flatMap({ $0 })
//    let trackItems = sections.map { char -> TrackItem in
//      let tracks = array.filter({ track in
//        let artChar = track.artist.characters.first
//        return char == artChar
//      })
//      return TrackItem(trackArray: tracks, andSectionHeader: String(char).uppercased())
//    }
//    return trackItems //array.sorted(by: { $0.artist < $1.artist })
//  }
//
//  var count: Int {
//    return array.count
//  }
//
//  func updateTracksModel(inRealm: RealmConfigurable) {
//    DispatchQueue.global(qos: .default).async {
//      autoreleasepool {
//        var realm: Realm
//        do {
//          switch inRealm {
//          case _ as TemporaryTrackStorage:
//            realm = try! Realm(configuration: TemporaryTrackStorage().configuration)
//          case _ as DefaultRealmStore:
//            realm = try! Realm()
//          default:
//            return
//          }
//          let storedTracks = self.array.map({ obj in
//            return realm.object(ofType: StoredTrack.self, forPrimaryKey: obj.id) ?? StoredTrack(track: obj)
//          })
//          realm.beginWrite()
//          storedTracks.forEach({ element in
//            realm.add(element, update: true)
//          })
//          try realm.commitWrite()
//        } catch let error as NSError {
//          print("Error writing all tracks to memory \(error.localizedDescription)")
//        }
//      }
//    }
//  }
//
//}
