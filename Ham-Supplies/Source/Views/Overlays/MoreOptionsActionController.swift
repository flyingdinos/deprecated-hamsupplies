//
//  MoreOptionsActionController.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/25/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation

//
//class MoreOptionsActionController: RMActionController<UIView> {
//
//  required override init?(style aStyle: RMActionControllerStyle, title aTitle: String?, message aMessage: String?, select selectAction: RMAction<UIView>?, andCancel cancelAction: RMAction<UIView>?) {
//    super.init(style: aStyle, title: aTitle, message: aMessage, select: selectAction, andCancel: cancelAction);
//
//    self.contentView = UIView(frame: .zero)
//    self.contentView.translatesAutoresizingMaskIntoConstraints = false
//
//    let bindings = ["contentView": self.contentView];
//    self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "[contentView(>=300)]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: bindings))
//    self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[contentView(200)]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: bindings))
//  }
//
//  required init?(coder aDecoder: NSCoder) {
//    super.init(coder: aDecoder);
//  }
//}
