//
//  PlaylistTrackTableViewCell.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/26/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class PlaylistTrackTableViewCell: MGSwipeTableCell {

  static let reuseIdentifier = "playlistTrackCell"
  static let nibIdentifier = "PlaylistTrackTableViewCell"
  static let nib = UINib(nibName: PlaylistTrackTableViewCell.nibIdentifier, bundle: nil)

  @IBOutlet weak var artistLabel: UILabel!
  @IBOutlet weak var trackLabel: UILabel!
  @IBOutlet weak var savedOfflineImageView: UIImageView!
//  @IBOutlet weak var containerView: UIView!
//  @IBOutlet weak var actionButton: UIButton!

  let savedOfflineImage = UIImage(icon: FontType.fontAwesome(.arrowCircleODown), size: CGSize(width: 25, height: 25), textColor: ColorPalatte.NeonBlue.belizeHole, backgroundColor: UIColor.clear)

  let checkedCircleImage = UIImage(icon: FontType.icofont(.checkCircled), size: CGSize(width: 25, height: 25), textColor: ColorPalatte.NeonBlue.belizeHole, backgroundColor: UIColor.clear)

  override func awakeFromNib() {
    super.awakeFromNib()

    savedOfflineImageView.image = savedOfflineImage
    savedOfflineImageView.contentMode = .center
    self.selectionStyle = .none
    let bgColorView = UIView()
    bgColorView.backgroundColor = ColorPalatte.Blue.midnightBlue
    self.selectedBackgroundView = bgColorView
    // Initialization code
  }

  override func layoutSubviews() {
    super.layoutSubviews()
    savedOfflineImageView?.isHidden = !isSavedOffline
  }

  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)

  }

  var isSavedOffline: Bool {
    return Folder.exists(withID: trackID, inFolder: Folder.savedTracks).found
  }

  var trackID: TrackID = ""
  var index: Int = -1

  var artist: String? {
    get {
      return artistLabel?.text
    }
    set {
      artistLabel?.text = newValue
    }
  }

  var title: String? {
    get {
      return trackLabel?.text
    }
    set {
      trackLabel?.text = newValue
    }
  }
}

//extension PlaylistTrackTableViewCell: MGSwipeTableCellDelegate {
//
//  func swipeTableCell(_ cell: MGSwipeTableCell, canSwipe direction: MGSwipeDirection, from point: CGPoint) -> Bool {
//    return direction == MGSwipeDirection.rightToLeft
//  }
//
//  func swipeTableCell(_ cell: MGSwipeTableCell, shouldHideSwipeOnTap point: CGPoint) -> Bool {
//    return true
//  }
//
//  func swipeTableCell(_ cell: MGSwipeTableCell, tappedButtonAt index: Int, direction: MGSwipeDirection, fromExpansion: Bool) -> Bool {
//    
//    switch (direction, index) {
//    case (MGSwipeDirection.rightToLeft, 0):
//      let realm = try! Realm()
//      playlist?.removeTrack(atIndex: index, inRealm: realm)
//      
//    default: break
//    }
//    return true
//  }
//}
