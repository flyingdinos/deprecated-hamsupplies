//
//  IndividualTrackCollectionViewCell.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/7/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import UIKit

class PlaylistCollectionViewCell: UICollectionViewCell {

  @IBOutlet weak var trackCountLabel: UILabel!
  @IBOutlet weak var playlistNameLabel: UILabel!
  @IBOutlet weak var containerView: UIView!
  @IBOutlet weak var actionButton: UIButton!

  let moreOptionsImage = UIImage(icon: FontType.fontAwesome(.angleRight), size: CGSize(width: 28, height: 28), textColor: ColorPalatte.Grey.fog, backgroundColor: UIColor.clear)

  override func awakeFromNib() {
    super.awakeFromNib()
    actionButton.backgroundColor = .clear
    actionButton.setTitle("", for: .normal)
    actionButton.setImage(moreOptionsImage, for: .normal)
  }

  @IBAction func touchedActionButton(_ button: UIButton) {
    print("Touched Action Button")
//    guard let selectedTrack = track else { return }
//    delegate?.touchedMoreOptions(forIndex: index, withTrackID: selectedTrack.id )
  }

  var playlist: Playlist? {
    didSet {
      if let item = playlist {
        playlistNameLabel?.text = item.name
        let countText = "\(item.tracks.count) Songs"
        trackCountLabel?.text = countText
      }
    }
  }
  var index: Int = -1

}
