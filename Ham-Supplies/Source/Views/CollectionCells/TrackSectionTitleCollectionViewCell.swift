//
//  TrackSectionTitleCollectionViewCell.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/21/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import UIKit
import IGListKit

import TinyConstraints

class TrackSectionTitleCollectionViewCell: UICollectionViewCell {

  override func awakeFromNib() {
      super.awakeFromNib()
      // Initialization code
  }

  fileprivate static let insets = UIEdgeInsets(top: 10, left: 15, bottom: 9, right: 15)
  fileprivate static let font = UIFont.systemFont(ofSize: 17)

  static var singleLineHeight: CGFloat {
    return font.lineHeight + insets.top + insets.bottom
  }

  static func textHeight(_ text: String, width: CGFloat) -> CGFloat {
    let constrainedSize = CGSize(width: width - insets.left - insets.right, height: CGFloat.greatestFiniteMagnitude)
    let attributes = [ NSAttributedStringKey.font: font ]
    let options: NSStringDrawingOptions = [.usesFontLeading, .usesLineFragmentOrigin]
    let bounds = (text as NSString).boundingRect(with: constrainedSize, options: options, attributes: attributes, context: nil)
    return ceil(bounds.height) + insets.top + insets.bottom
  }

  @IBOutlet weak var titleLabel: UILabel!
  
  @IBOutlet weak var actionButton: UIButton!
  
  let clearSectionImage = UIImage(icon: FontType.ionicons(.closeCircled), size: CGSize(width: 25, height: 25), textColor: ColorPalatte.Grey.concrete, backgroundColor: .clear)

  var text: String? {
    get {
      return titleLabel?.text
    }
    set {
      titleLabel?.text = newValue
    }
  }
  
  enum ActionButton {
    case none
    case clear
  }
  
  func setActionButton(to buttonType: ActionButton) {
    guard buttonType != ActionButton.none else { return }
    
    actionButton = UIButton(type: .custom)
    actionButton.setImage(clearSectionImage, for: .normal)
    let height = contentView.frame.size.height
    actionButton.top(to: contentView)
    actionButton.bottom(to: contentView)
    
  }

  override init(frame: CGRect) {
    super.init(frame: frame)

    titleLabel?.backgroundColor = .clear
    titleLabel?.numberOfLines = 0
    titleLabel?.font = TrackSectionTitleCollectionViewCell.font

    contentView.backgroundColor = .clear
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func layoutSubviews() {
    super.layoutSubviews()
    let bounds = contentView.bounds
    titleLabel.frame = UIEdgeInsetsInsetRect(bounds, TrackSectionTitleCollectionViewCell.insets)
    
//    let height: CGFloat = 0.5
//    let left = TrackSectionTitleCollectionViewCell.insets.left
//    separator.frame = CGRect(x: left, y: bounds.height - height, width: bounds.width - left, height: height)
  }

  override var isHighlighted: Bool {
    didSet {
      contentView.backgroundColor = UIColor(white: isHighlighted ? 0.9 : 1, alpha: 1)
    }
  }

}

extension TrackSectionTitleCollectionViewCell: ListBindable {

  func bindViewModel(_ viewModel: Any) {
    guard let viewModel = viewModel as? String else { return }
    titleLabel.text = viewModel
  }

}
