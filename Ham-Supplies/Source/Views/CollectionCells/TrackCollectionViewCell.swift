//
//  TrackCollectionViewCell.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/6/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import UIKit

class TrackCollectionViewCell: UICollectionViewCell {

  @IBOutlet weak var artistLabel: UILabel!
  @IBOutlet weak var trackLabel: UILabel!

  override func awakeFromNib() {
    super.awakeFromNib()
  }

  var artist: String? {
    get {
      return artistLabel.text
    }
    set {
      artistLabel.text = newValue
    }
  }

  var title: String? {
    get {
      return trackLabel.text
    }
    set {
      trackLabel.text = newValue
    }
  }

}
