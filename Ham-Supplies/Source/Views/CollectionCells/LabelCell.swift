/**
 Copyright (c) 2016-present, Facebook, Inc. All rights reserved.

 The examples provided by Facebook are for non-commercial testing and evaluation
 purposes only. Facebook reserves all rights not expressly granted.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 FACEBOOK BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import UIKit
import IGListKit

final class LabelCell: UICollectionViewCell {

  fileprivate static let insets = UIEdgeInsets(top: 10, left: 15, bottom: 9, right: 15)
  fileprivate static let font = UIFont.systemFont(ofSize: 17)

  static var singleLineHeight: CGFloat {
    return font.lineHeight + insets.top + insets.bottom
  }

  static func textHeight(_ text: String, width: CGFloat) -> CGFloat {
    let constrainedSize = CGSize(width: width - insets.left - insets.right, height: CGFloat.greatestFiniteMagnitude)
    let attributes = [NSAttributedStringKey.font: font]
    let options: NSStringDrawingOptions = [.usesFontLeading, .usesLineFragmentOrigin]
    let bounds = (text as NSString).boundingRect(with: constrainedSize, options: options, attributes: attributes, context: nil)
    return ceil(bounds.height) + insets.top + insets.bottom
  }

  var label: UILabel = {
    let label = UILabel()
    label.backgroundColor = .clear
    label.numberOfLines = 0
    label.font = LabelCell.font
    return label
  }()

  var separator: CALayer = {
    let layer = CALayer()
    layer.backgroundColor = ColorPalatte.Grey.concrete.cgColor
    return layer
  }()

  var text: String? {
    get {
      return label.text
    }
    set {
      label.text = newValue
    }
  }

  override init(frame: CGRect) {
    super.init(frame: frame)
    contentView.addSubview(label)
    contentView.layer.addSublayer(separator)
    contentView.backgroundColor = contentViewColor
  }

  var contentViewColor: UIColor = ColorPalatte.Blue.midnightBlue {
    didSet {
      contentView.backgroundColor = contentViewColor
    }
  }

  var textColor: UIColor = ColorPalatte.Grey.zinc {
    didSet {
      label.textColor = textColor
    }
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func layoutSubviews() {
    super.layoutSubviews()
    let bounds = contentView.bounds
    label.frame = UIEdgeInsetsInsetRect(bounds, LabelCell.insets)
    let height: CGFloat = 0.75
    let left = LabelCell.insets.left
    separator.frame = CGRect(x: left, y: bounds.height - height, width: bounds.width - left, height: height)
  }

  override var isHighlighted: Bool {
    didSet {
      contentView.backgroundColor = UIColor(white: isHighlighted ? 0.9 : 1, alpha: 1)
    }
  }

}

extension LabelCell: ListBindable {

  func bindViewModel(_ viewModel: Any) {
    guard let viewModel = viewModel as? String else { return }
    label.text = viewModel
  }

}
