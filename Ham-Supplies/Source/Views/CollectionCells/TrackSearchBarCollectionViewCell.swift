//
//  TrackSearchCollectionViewCell.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/29/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import UIKit

class TrackSearchBarCollectionViewCell: UICollectionViewCell {

  @IBOutlet weak var searchBar: UISearchBar!
  @IBOutlet weak var containerView: UIView!

  static let reuseIdentifier = "TrackSearchBarCollectionViewCell"
  static let nibName = "TrackSearchBarCollectionViewCell"
  static let nib = UINib(nibName: nibName, bundle: nil)

  override func awakeFromNib() {
    super.awakeFromNib()
  }

  func setColors() {
    self.containerView.backgroundColor = ColorPalatte.Blue.wetAsphalt
    self.searchBar.barTintColor = ColorPalatte.Blue.wetAsphalt
    UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).backgroundColor = ColorPalatte.Grey.clouds
  }

}
