//
//  IndividualTrackCollectionViewCell.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/7/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import UIKit

class GenericTrackFilterCell: UICollectionViewCell {

  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var trackCountLabel: UILabel!
  @IBOutlet weak var containerView: UIView!
  @IBOutlet weak var actionButton: UIButton!

  let moreOptionsImage = UIImage(icon: FontType.fontAwesome(.angleRight), size: CGSize(width: 28, height: 28), textColor: ColorPalatte.Grey.fog, backgroundColor: UIColor.clear)

  override func awakeFromNib() {
    super.awakeFromNib()
    actionButton.backgroundColor = .clear
    actionButton.setTitle("", for: .normal)
    actionButton.setImage(moreOptionsImage, for: .normal)
  }

  @IBAction func touchedActionButton(_ button: UIButton) {
    print("Touched Action Button")
//    guard let selectedTrack = track else { return }
//    delegate?.touchedMoreOptions(forIndex: index, withTrackID: selectedTrack.id )
  }

  var songCount: Int = 0 {
    didSet {
      trackCountLabel?.text = "\(songCount) Songs"
    }
  }
  
  var title: String? {
    get {
      return titleLabel?.text
    }
    set {
      titleLabel?.text = newValue
    }
  }
  
  var index: Int = -1

}
