//
//  IndividualTrackCollectionViewCell.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/7/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import UIKit

import IGListKit

typealias TrackID = String

protocol TrackOptionsDelegate: class {
  func touchedMoreOptions(forIndex index: Int, withTrackID trackID: TrackID)
}

class IndividualTrackCollectionViewCell: UICollectionViewCell {

  @IBOutlet weak var artistLabel: UILabel!
  @IBOutlet weak var trackLabel: UILabel!
  @IBOutlet weak var containerView: UIView!
  @IBOutlet weak var actionButton: UIButton!

  weak var delegate: TrackOptionsDelegate?

  let buttonImage = UIImage(icon: FontType.fontAwesome(.ellipsisH), size: CGSize(width: 28, height: 28), textColor: ColorPalatte.Grey.fog, backgroundColor: UIColor.clear)
  let moreOptionsImage = UIImage(icon: FontType.ionicons(.more), size: CGSize(width: 28, height: 28), textColor: ColorPalatte.Grey.fog, backgroundColor: UIColor.clear)

  override func awakeFromNib() {
    super.awakeFromNib()
    actionButton.backgroundColor = .clear
    actionButton.setTitle("", for: .normal)
    actionButton.setImage(moreOptionsImage, for: .normal)
  }

  @IBAction func touchedActionButton(_ button: UIButton) {
    print("Touched Action Button")
    delegate?.touchedMoreOptions(forIndex: index, withTrackID: storedTrack.id )
  }

  var cellViewModel: StoredTrackViewModel? {
    didSet {
      if let viewModel = cellViewModel {
        storedTrack = viewModel.track
        index = viewModel.index
      }
    }
  }

  var storedTrack: StoredTrack = StoredTrack() {
    didSet {
      artist = storedTrack.artist
      title = storedTrack.title
    }
  }

  var index: Int = -1

  var artist: String? {
    get {
      return artistLabel?.text
    }
    set {
      artistLabel?.text = newValue
    }
  }

  var title: String? {
    get {
      return trackLabel?.text
    }
    set {
      trackLabel?.text = newValue
    }
  }

}

extension IndividualTrackCollectionViewCell: ListBindable {

  func bindViewModel(_ viewModel: Any) {
    guard let viewModel = viewModel as? StoredTrackViewModel else { return }
    self.cellViewModel = viewModel
  }

}
