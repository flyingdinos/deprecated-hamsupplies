//
//  TrackItemSectionHeaderCollectionViewCell.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/29/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import UIKit

class TrackItemSectionHeaderCollectionViewCell: UICollectionViewCell {

  @IBOutlet weak var containerView: UIView!

  fileprivate static let insets = UIEdgeInsets(top: 10, left: 15, bottom: 9, right: 15)
  fileprivate static let font = UIFont.systemFont(ofSize: 17)

  static var singleLineHeight: CGFloat {
    return font.lineHeight + insets.top + insets.bottom
  }

  static func textHeight(_ text: String, width: CGFloat) -> CGFloat {
    let constrainedSize = CGSize(width: width - insets.left - insets.right, height: CGFloat.greatestFiniteMagnitude)
    let attributes = [ NSAttributedStringKey.font: font ]
    let options: NSStringDrawingOptions = [.usesFontLeading, .usesLineFragmentOrigin]
    let bounds = (text as NSString).boundingRect(with: constrainedSize, options: options, attributes: attributes, context: nil)
    return ceil(bounds.height) + insets.top + insets.bottom
  }

  var label: UILabel = {
    let label = UILabel()
    label.backgroundColor = .clear
    label.numberOfLines = 0
    label.font = TrackItemSectionHeaderCollectionViewCell.font
    return label
  }()

  var separator: CALayer = {
    let layer = CALayer()
    layer.backgroundColor = ColorPalatte.Grey.concrete.cgColor
    return layer
  }()

  var text: String? {
    get {
      return label.text
    }
    set {
      label.text = newValue
    }
  }

  override init(frame: CGRect) {
    super.init(frame: frame)
    containerView.backgroundColor = ColorPalatte.Blue.midnightBlue
    containerView.addSubview(label)
    containerView.layer.addSublayer(separator)
    containerView.backgroundColor = .clear
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
//    fatalError("init(coder:) has not been implemented")
  }

  override func layoutSubviews() {
    super.layoutSubviews()
    let bounds = containerView.bounds
    label.frame = UIEdgeInsetsInsetRect(bounds, TrackItemSectionHeaderCollectionViewCell.insets)
    let height: CGFloat = 0.75
    let left = TrackItemSectionHeaderCollectionViewCell.insets.left
    separator.frame = CGRect(x: left, y: bounds.height - height, width: bounds.width - left, height: height)
  }

  override var isHighlighted: Bool {
    didSet {
      contentView.backgroundColor = UIColor(white: isHighlighted ? 0.9 : 1, alpha: 1)
    }
  }

}
