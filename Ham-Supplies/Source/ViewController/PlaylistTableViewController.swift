//
//  PlaylistTableViewController.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/26/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import UIKit
import Realm
import RealmSwift
import MGSwipeTableCell
import Closures
import Disk

import NotificationCenter

class RealmHelper {

  static func objects<T: Object>(type: T.Type) -> Results<T>? {
    let realm = try? Realm()

    return realm?.objects(type)
  }

  static func object<T: Object>(type: T.Type, andKey key: Int) -> T? {
    let realm = try? Realm()
    return realm?.object(ofType: type, forPrimaryKey: key)
  }
}

class PlaylistTableViewController: ViewController {

  @IBOutlet weak var tableView: UITableView!

  lazy var playlist: Playlist? = RealmHelper.object(type: Playlist.self, andKey: self.playlistID)
  var notificationToken: NotificationToken?
  var playlistID: Int = -1
  let notification = LocalNotification.shared

  let checkedCircleImage = UIImage(icon: FontType.fontAwesome(.cloudDownload), size: CGSize(width: 35, height: 35), textColor: ColorPalatte.NeonBlue.peterRiver, backgroundColor: UIColor.clear)

  lazy var saveOffline: UIBarButtonItem = {
    let button = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(savePlaylistOffline))
    return button
  }()

  lazy var saveOfflineDownloadButton: UIBarButtonItem = {
    let button = UIBarButtonItem(image: self.checkedCircleImage, style: .plain, target: self, action: #selector(savePlaylistOffline))
    button.tintColor = ColorPalatte.NeonBlue.peterRiver
//    let button = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(savePlaylistOffline))
    return button
  }()

  init(playlistPrimaryKey: Int) {
    super.init(nibName: "PlaylistTableViewController", bundle: nil)
    self.playlistID = playlistPrimaryKey
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.register(PlaylistTrackTableViewCell.nib, forCellReuseIdentifier: PlaylistTrackTableViewCell.reuseIdentifier)
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = 50
    tableView.delegate = self
    tableView.dataSource = self
    tableView.backgroundColor = ColorPalatte.Blue.moonlight

    let name = playlist?.name ?? ""
    self.setTitleInNavBar(withTitle: name)
    self.styleNavigationBar()
    self.navigationItem.rightBarButtonItem = saveOfflineDownloadButton

    notificationToken = playlist?.tracks.observe { [weak self] (changes: RealmCollectionChange) in
      switch changes {
      case .initial:
        // Results are now populated and can be accessed without blocking the UI
        self?.tableView?.reloadData()
      case .update:
        // Query results have changed, so apply them to the UITableView
        self?.tableView?.reloadData()
      case .error(let error):
        // An error occurred while opening the Realm file on the background worker thread
        fatalError("\(error)")
      }
    }
  }

  deinit {
    print("Deiniting Playlist Table View Controller")
    LocalNotification.shared.center.removeObserver(self, name: NotificationKey.trackSavedOffline.notificationName, object: nil)
    notificationToken?.invalidate()
  }

  func reloadCell(forTrackID trackID: TrackID) {
    guard let tracks = playlist?.tracks else {
      return
    }
    for (index, track) in tracks.enumerated() where track.id == trackID {
      let indexPath = [IndexPath(row: index, section: 0)]
      tableView.reloadRows(at: indexPath, with: .automatic)
    }
  }

  @objc func trackSavedOfflineNotification(_ notification: Notification) {
    print(notification.object ?? "")
    if let dict = notification.userInfo,
      let trackID = dict["trackID"] as? String {
      DispatchQueue.main.async { [weak self] in
        self?.reloadCell(forTrackID: trackID)
      }
    }
  }

  @objc func savePlaylistOffline() {
    notification.center.addObserver(self, selector: #selector(trackSavedOfflineNotification(_:)), name: NotificationKey.trackSavedOffline.notificationName, object: nil)
    print("Saving Offline")
//    playlist?.saveTracksForOffline(true)
    playlist?.tracks.forEach({ track in
      print("Track Loop")
      let offlineFile = track.offlineFileName
      let trackID = track.id
      let convertedFileName = track.convertedFileName
      let searchResults = Disk.search(forFile: offlineFile, convertedFileName: convertedFileName)
      print(searchResults)
      switch searchResults {
      case let res where res.found && res.folder == Folder.savedTracks:
        print("Track \(trackID) found in Saved Tracks")
      case let res where res.found && res.folder == Folder.temporary:
        print("Track \(trackID) found in Temp Directory")
        DispatchQueue.global(qos: .userInitiated).async {
          NetworkManager.compressAndSave(trackID: trackID, fromLocalURL: searchResults.fileURL)
        }
      case let res where !res.found:
        if let remoteURL = track.songRemoteURL {
        DispatchQueue.global(qos: .userInitiated).async {
            print("Downloading \(trackID) for offline")
            NetworkManager.downloadAndSaveSongOffline(forTrackId: trackID, offlineFileName: offlineFile, remoteURL: remoteURL)
          }
        }
      default:
        break
      }
    })
  }

  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    return UIView()
  }

  var trackCount: Int {
    return playlist?.tracks.count ?? 0
  }

}

extension PlaylistTableViewController: UITableViewDelegate {

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    guard indexPath.row < trackCount,
      let tracks = playlist?.tracks else { return }
    let firstTrack = tracks[indexPath.row]
    var trackList: [String] = tracks.filter({ $0.id != firstTrack.id })
      .map({ val -> String in
        return val.id
      }).compactMap({ $0 })
    trackList.insert(firstTrack.id, at: 0)
    TrackQueueBus.shared.playPlaylist(trackList)
  }

}

extension PlaylistTableViewController: UITableViewDataSource {

//  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//    return 50
//  }

  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return trackCount
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: PlaylistTrackTableViewCell.reuseIdentifier,
                                                   for: indexPath) as? PlaylistTrackTableViewCell else {
      fatalError("Could not cast to playlist track table view cell")
    }
    guard let track = playlist?.tracks[indexPath.row] else {
      return UITableViewCell()
    }

    cell.artist = track.artist
    cell.title = track.title
    cell.index = indexPath.row
    cell.trackID = track.id
    cell.delegate = self
    cell.rightButtons = [MGSwipeButton(title: "Delete", backgroundColor: .red)]
    cell.rightSwipeSettings.transition = .rotate3D
    return cell
  }

}

extension PlaylistTableViewController: MGSwipeTableCellDelegate {

  func swipeTableCell(_ cell: MGSwipeTableCell, canSwipe direction: MGSwipeDirection, from point: CGPoint) -> Bool {
    return direction == MGSwipeDirection.rightToLeft
  }

  func swipeTableCell(_ cell: MGSwipeTableCell, shouldHideSwipeOnTap point: CGPoint) -> Bool {
    return true
  }

  func swipeTableCell(_ cell: MGSwipeTableCell, tappedButtonAt index: Int, direction: MGSwipeDirection, fromExpansion: Bool) -> Bool {

    switch (direction, index) {
    case (MGSwipeDirection.rightToLeft, 0):
      let realm = try! Realm()
      playlist?.removeTrack(atIndex: index, inRealm: realm, withoutNotifying: notificationToken)

    default: break
    }
    return true
  }

}
