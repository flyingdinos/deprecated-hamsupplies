//
//  ViewController.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/30/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation
import UIKit
//#if DEBUG
//  import FLEX
//#endif

protocol DebuggableShake: class {
  var showingExplorer: Bool { get set }
}

class ViewController: UIViewController {

  var showingExplorer: Bool = false
}

  extension ViewController: DebuggableShake {

    override var canBecomeFirstResponder: Bool {
      return true
    }

    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
      if motion == .motionShake {
        showingExplorer = !showingExplorer
        showingExplorer ? FLEXManager.shared().showExplorer() : FLEXManager.shared().hideExplorer()
      }
    }
  }
