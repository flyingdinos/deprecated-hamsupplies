//
//  MixedHomeViewController.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/4/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import UIKit
import IGListKit
import RxSwift
import DynamicColor
import UIFontComplete
import CZPicker
import RealmSwift
import Realm
import RxCocoa

enum TrackSegment: Int {
  case featured
  case all
  case artist
  case genre
  case search
}

final class MixedDataViewController: ViewController, ListAdapterDataSource {

  lazy var adapter: ListAdapter = {
    let adapt = ListAdapter(updater: ListAdapterUpdater(), viewController: self)
    return adapt
  }()

  var filterString = Variable("")
  let searchToken: NSNumber = 42
  var selectedSegment = TrackSegment.featured
  var disposeBag = DisposeBag()
  let font: UIFont! = UIFont(font: Font.helvetica, size: 13)
  var playlistResults: Results<Playlist>?

  let collectionView = UICollectionView(frame: .zero, collectionViewLayout: ListCollectionViewLayout(stickyHeaders: true, topContentInset: 0.0, stretchToEdge: false))

  var featuredTrackData: [StoredTrackItem] {
    return [StoredTrackItem(trackResults: self.mostRecentTracks, limit: 15, andSectionHeader: "Recently Added"),
    StoredTrackItem(trackResults: self.mostRecentTracks, limit: 15, andSectionHeader: "Recently Played")]
  }

  var allTracks: [StoredTrackItem] {
    return [StoredTrackItem(trackResults: storedTracks.sorted(byKeyPath: "title", ascending: true), andSectionHeader: "All")]
  }

//  lazy var artists: [Any] = [TrackItem(trackArray: TrackCollection.shared.trackSort(), andSectionHeader: "All")]

  lazy var searchData: [Any] = [searchToken, TrackItem(trackArray: [], andSectionHeader: "")]

  let segments: [(String, TrackSegment)] = [
    ("Featured", TrackSegment.featured),
    ("All", TrackSegment.all),
    ("Artist", TrackSegment.artist),
    ("Genre", TrackSegment.genre),
    ("Search", TrackSegment.search)
  ]

  let refreshImage = UIImage(icon: FontType.fontAwesome(.refresh), size: CGSize(width: 25, height: 25), textColor: ColorPalatte.NeonBlue.peterRiver, backgroundColor: UIColor.clear)

  lazy var refreshLibraryButton: UIBarButtonItem = {
    let button = UIBarButtonItem(image: self.refreshImage, style: .plain, target: self, action: #selector(refreshLibrary))
    button.tintColor = ColorPalatte.NeonBlue.peterRiver
    return button
  }()

  @objc func refreshLibrary() {
    NetworkManager.downloadLatestLibrary(completion: { (success, libraryFileURL) in
      if success, let libraryURL = libraryFileURL {
        StoredTrack.updateLibrary(withLibraryURL: libraryURL)
      }
    })
  }

  var storedTrackCount: Int {
    return storedTracks.count
  }

  lazy var storedTracks: Results<StoredTrack> = StoredTrack.allTracks()

  var allArtists: [GenericCollection] {
    let artists = Set(self.storedTracks
      .map({ $0.artist as String }))
      .sorted()
      
    let items = artists
      .map({ artist -> GenericItem in
        let predicate = NSPredicate(format: "artist = %@", artist)
        let count = self.storedTracks.filter(predicate).count
        return GenericItem(title: artist, count: count)
      })
    
    return [GenericCollection(items: items, count: items.count)]
  }
  
  var allGenres: [GenericCollection] {
    let genres = Set(self.storedTracks
      .map({ $0.genre as String }))
      .sorted()
    
    let items = genres
      .map({ genre -> GenericItem in
        let predicate = NSPredicate(format: "genre = %@", genre)
        let count = self.storedTracks.filter(predicate).count
        return GenericItem(title: genre, count: count)
      })
    
    return [GenericCollection(items: items, count: items.count)]
  }
  
  var mostRecentTracks: Results<StoredTrack> {
    return self.storedTracks
      .sorted(byKeyPath: "dateAdded", ascending: false)
  }

  var recentlyPlayedTracks: Results<StoredTrack> {
    guard self.storedTracks.sum(ofProperty: "playCount") > 3 else {
      return self.storedTracks.filter("title == 'zzzzzzzzz'")
    }
    return self.storedTracks
      .sorted(byKeyPath: "lastPlayed", ascending: false)
  }
  var control: UISegmentedControl = UISegmentedControl()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
    
    collectionView.backgroundColor = AppTheme.backgroundColor
    control = UISegmentedControl(items: segments.map { return $0.0 })
    control.selectedSegmentIndex = 0
    control.tintColor = ColorPalatte.NeonBlue.peterRiver
    let selectedColor = [NSAttributedStringKey.font: font, NSAttributedStringKey.foregroundColor: ColorPalatte.Grey.fog] as [NSAttributedStringKey: Any]
    control.setTitleTextAttributes(selectedColor, for: .selected)
    control.setTitleTextAttributes(selectedColor, for: .normal)
    control.addTarget(self, action: #selector(MixedDataViewController.onControl(_:)), for: .valueChanged)
    navigationItem.titleView = control
    self.navigationItem.leftBarButtonItem = refreshLibraryButton

    styleNavigationBar()
    view.addSubview(collectionView)
    adapter.collectionView = collectionView
    adapter.dataSource = self

    filterString.asObservable()
      .skip(1)
      .distinctUntilChanged()
      .subscribe(onNext: { text in
        self.searchData = self.search(forText: text)
        self.adapter.performUpdates(animated: true, completion: nil)
      }).disposed(by: disposeBag)

  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.becomeFirstResponder()
    if let flowLayout = collectionView.collectionViewLayout as? ListCollectionViewLayout {
      flowLayout.stickyHeaderYOffset = self.topLayoutGuide.length
      collectionView.collectionViewLayout = flowLayout
    }
  }

  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    collectionView.frame = view.bounds
  }

  @objc func onControl(_ control: UISegmentedControl) {
    selectedSegment = segments[control.selectedSegmentIndex].1
    adapter.performUpdates(animated: true, completion: nil)
  }

  // MARK: ListAdapterDataSource

  func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
    switch selectedSegment {
    case .featured:
      return featuredTrackData
    case .all:
      return allTracks //.map { $0 as? ListDiffable }.flatMap({ $0 })
    case .artist:
      return allArtists.compactMap({ $0 as ListDiffable })
    case .genre:
      return allGenres.compactMap({ $0 as ListDiffable })
    case .search:
      return searchData.compactMap { $0 as? ListDiffable }
    }
  }

  func search(forText text: String) -> [ListDiffable] {
    let predicate = NSPredicate(format: "searchString CONTAINS %@", text)
    let results = storedTracks.filter(predicate)
    let searchResults: [Any] = [searchToken, StoredTrackItem(trackResults: results, andSectionHeader: "")]
    return searchResults.map({ $0 as? ListDiffable }).compactMap({ $0 })
  }

  func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
    switch object {
    case _ where selectedSegment == .search:
      if let obj = object as? NSNumber, obj == searchToken {
        let sectionController = SearchSectionController(withSearchText: filterString.value)
        sectionController.delegate = self
        sectionController.displayDelegate = self
        return sectionController
      } else {
        return StoredTrackSectionController()
      }
    case is String:   return ExpandableSectionController()
    case is RecentTrackItem:
      let section = TrackHorizontalSectionController()
      section.displayDelegate = self
      return section
    case is StoredTrackItem:
      let section = StoredTrackSectionController()
      section.displayDelegate = self
      return section
    case is GenericCollection:
      let section = GenericItemSectionController()
      section.selectedDelegate = self
      return section
    default: return ExpandableSectionController()
    }
  }

  func emptyView(for listAdapter: ListAdapter) -> UIView? { return nil }
}

extension MixedDataViewController: ListSingleSectionControllerDelegate {

  func didSelect(_ sectionController: ListSingleSectionController, with object: Any) {
    let section = adapter.section(for: sectionController) + 1
    let alert = UIAlertController(title: "Section \(section) was selected \u{1F389}",
      message: "Cell Object: " + String(describing: object),
      preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
    present(alert, animated: true, completion: nil)
  }

}

extension MixedDataViewController: GenericItemSelectedDelegate {
  
  func selectedItem(withKey: String, atIndex index: Int) {
    self.filterString.value = withKey
    control.selectedSegmentIndex = TrackSegment.search.rawValue
    onControl(control)
//    selectedSegment = .search
//    adapter.performUpdates(animated: true, completion: { _ in
//      self.filterString.value = withKey
//    })
//    adapter.performUpdates(animated: true, completion: { com in
//      print("THINGY")
//    })
  }
}

extension MixedDataViewController: ListDisplayDelegate {
  func listAdapter(_ listAdapter: ListAdapter, willDisplay sectionController: ListSectionController) {
    switch sectionController {
    case is SearchSectionController:
      guard let searchSection = sectionController as? SearchSectionController else { return }
      searchSection.delegate?.searchSectionController(searchSection, didChangeText: filterString.value)
    default:
      break
    }
  }

  func listAdapter(_ listAdapter: ListAdapter, didEndDisplaying sectionController: ListSectionController) {

  }

  func listAdapter(_ listAdapter: ListAdapter, willDisplay sectionController: ListSectionController, cell: UICollectionViewCell, at index: Int) {

  }

  func listAdapter(_ listAdapter: ListAdapter, didEndDisplaying sectionController: ListSectionController, cell: UICollectionViewCell, at index: Int) {

  }

}

extension MixedDataViewController: SearchSectionControllerDelegate {

  func searchSectionController(_ sectionController: SearchSectionController, didChangeText text: String) {
    filterString.value = text.lowercased()
  }

}
