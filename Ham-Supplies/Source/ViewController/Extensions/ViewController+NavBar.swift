//
//  ViewController+NavBar.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/25/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {

  func styleNavigationBar() {
    self.navigationController?.navigationBar.tintColor = ColorPalatte.Grey.fog
    self.navigationController?.navigationBar.barTintColor = ColorPalatte.Blue.moonlight
    self.navigationController?.navigationBar.layer.shadowColor = ColorPalatte.Blue.darkness.cgColor
    self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
    self.navigationController?.navigationBar.layer.shadowRadius = 3.0
    self.navigationController?.navigationBar.layer.shadowOpacity = 1.0
    self.navigationController?.navigationBar.layer.masksToBounds = false
  }

  func setTitleInNavBar(withTitle navTitle: String) {
    self.navigationItem.title = navTitle
    self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: ColorPalatte.Grey.fog]
  }
}
