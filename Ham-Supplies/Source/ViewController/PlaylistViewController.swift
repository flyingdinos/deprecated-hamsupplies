//
//  PlaylistViewController.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/22/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import UIKit
import IGListKit
import RealmSwift

import MessageUI

class PlaylistViewController: ViewController {

  lazy var adapter: ListAdapter = {
    return ListAdapter(updater: ListAdapterUpdater(), viewController: self)
  }()

  let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
  let addButtonImage = UIImage(icon: FontType.fontAwesome(.plus), size: CGSize(width: 30, height: 30))

  lazy var addPlaylistButton: UIBarButtonItem = {
    let addPlaylist = UIBarButtonItem(image: addButtonImage, style: .plain, target: self, action: #selector(touchedAddPlaylist))
    return addPlaylist
  }()

  lazy var exportPlaylistButton: UIBarButtonItem = {
    let exportPlaylistButton = UIBarButtonItem(title: "Email", style: .plain, target: self, action: #selector(exportPlaylist))
    return exportPlaylistButton
  }()

  var playlistResults: Results<Playlist>?
  var notificationToken: NotificationToken?

  @objc func exportPlaylist() {
    if MFMailComposeViewController.canSendMail() {
      let mail = MFMailComposeViewController()
      mail.mailComposeDelegate = self
      mail.setToRecipients(["mike@mikesabo.com"])
      mail.setMessageBody("<p>Realm Database is attached here</p>", isHTML: true)
      guard let realmURL = Realm.Configuration.defaultConfiguration.fileURL,
      let realmData = try? Data(contentsOf: realmURL) else {
        return
      }

      mail.addAttachmentData(realmData, mimeType: "application/realm", fileName: "ham_supplies.realm")
      present(mail, animated: true)
    } else {
      // show failure alert
    }
  }

  @objc func touchedAddPlaylist() {
    print("Touched Add Playlist")
    let alertController = UIAlertController(title: "New Playlist", message: "Just type something already", preferredStyle: .alert)

    alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

    alertController.addAction(UIAlertAction(title: "Save", style: .default, handler: {
        _ -> Void in
      if let newPlaylistName = alertController.textFields?[0].text,
        newPlaylistName.count > 1 {
        let realm = try! Realm()
        let newPlaylist = Playlist()
        newPlaylist.name = newPlaylistName
        newPlaylist.save(inRealm: realm)
        print(newPlaylistName)
      }
    }))

    alertController.addTextField(configurationHandler: { (textField) -> Void in
      textField.placeholder = ""
      textField.textAlignment = .left
      textField.autocapitalizationType = .words
    })

    self.present(alertController, animated: true, completion: nil)
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationItem.rightBarButtonItem = addPlaylistButton
    self.navigationItem.leftBarButtonItem = exportPlaylistButton
    self.setTitleInNavBar(withTitle: "Playlists")
    self.styleNavigationBar()
    view.addSubview(collectionView)
    adapter.collectionView = collectionView
    adapter.dataSource = self

    let realm = try! Realm()
    playlistResults = realm.objects(Playlist.self)
    notificationToken = playlistResults?.observe { [weak self] (changes: RealmCollectionChange) in
      guard let `self` = self else { return }
      self.playlistData = [PlaylistItem(playlists: self.playlistResults?.compactMap({ $0 }), itemCount: self.playlistCount)]
      switch changes {
      case .initial:
        // Results are now populated and can be accessed without blocking the UI
        self.adapter.performUpdates(animated: true, completion: nil)
      case .update:
        // Query results have changed, so apply them to the UITableView
        self.adapter.performUpdates(animated: true, completion: nil)
      case .error(let error):
        // An error occurred while opening the Realm file on the background worker thread
        fatalError("\(error)")
      }
    }
  }

  deinit {
    notificationToken?.invalidate()
  }

  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    collectionView.frame = view.bounds
    adapter.collectionView = collectionView
    adapter.dataSource = self
  }

  var playlistCount: Int {
    return playlistResults?.count ?? 0
  }

  lazy var playlistData: [Any] = [PlaylistItem()]

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.becomeFirstResponder()
  }

}

extension PlaylistViewController: MFMailComposeViewControllerDelegate {

  func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
    controller.dismiss(animated: true)
  }

}

extension PlaylistViewController: PlaylistSelectedDelegate {

  func selectedPlaylist(withPrimaryKey: Int, atIndex index: Int) {
    let playlistVC = PlaylistTableViewController(playlistPrimaryKey: withPrimaryKey)
    self.navigationController?.pushViewController(playlistVC, animated: true)
  }

}

// MARK: ListAdapterDataSource

extension PlaylistViewController: ListAdapterDataSource {

  func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
    return playlistData.map({ $0 as? ListDiffable }).compactMap({ $0 }) // ?? [ListDiffable]()
  }

  func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
    switch object {
    case is PlaylistItem:
      let playlistSection = PlaylistSectionController()
      playlistSection.selectedDelegate = self
      return playlistSection
    default: return PlaylistSectionController()
    }
  }

  func emptyView(for listAdapter: ListAdapter) -> UIView? { return nil }
}
