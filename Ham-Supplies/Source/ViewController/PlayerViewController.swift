//
//  SecondViewController.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/1/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import UIKit
import RxSwift
import CoreAudio
import AVFoundation
import RealmSwift
import Realm
import MediaPlayer

let remoteCommandCenter = MPRemoteCommandCenter.shared()

class PlayerViewController: ViewController {

  static let buttonSize = CGSize(width: 35, height: 28)
  static let smallerButtonSize = CGSize(width: 35, height: 28)
  
  @IBOutlet weak var artistLabel: UILabel!
  @IBOutlet weak var songTitleLabel: UILabel!
  @IBOutlet weak var trackDurationLabel: UILabel!
  @IBOutlet weak var trackPositionLabel: UILabel!
  @IBOutlet weak var trackArtworkImageView: UIImageView!
  @IBOutlet weak var playSongButton: UIButton!
  @IBOutlet weak var songProgressView: UISlider!
  @IBOutlet weak var previousSongButton: UIButton!
  @IBOutlet weak var nextSongButton: UIButton!

  @IBAction func previousButtonTapped(_ sender: UIButton) {
    guard playlist.hasPrevious else { return }
//    store.dispatch(TrackQueuePreviousTrack())
    playlist.playPreviousSong()
  }

  @IBAction func nextButtonTapped(_ sender: UIButton) {
    guard playlist.hasNext else { return }
//    store.dispatch(TrackQueueSongFinished())
    playlist.nextSong()
  }

  @IBAction func playButtonTapped(_ button: UIButton) {
    if audioPlayer.isPlaying {
      pauseAudio()
    } else {
      playOrResumeAudio()
    }
  }
  @IBAction func songPositionChanged(_ sender: UISlider) {
    audioManager.seek(toTime: sender.value)
  }

  func pauseAudio() {
    audioManager.pause()
    playSongButton.setImage(playImage, for: .normal)
  }

  func playOrResumeAudio() {
    audioManager.resume()
    playSongButton.setImage(pauseImage, for: .normal)
  }

  var audioManager = AudioPlayer.instance
  var audioPlayer = AudioPlayer.instance.assetPlayer
  var playlist = TrackQueueBus.shared
  var disposeBag: DisposeBag! = DisposeBag()

  let playImage = UIImage(icon: FontType.fontAwesome(.play), size: PlayerViewController.buttonSize, textColor: ColorPalatte.Grey.fog, backgroundColor: UIColor.clear)
  let pauseImage = UIImage(icon: FontType.fontAwesome(.pause), size: PlayerViewController.buttonSize, textColor: ColorPalatte.Grey.fog, backgroundColor: UIColor.clear)
  let nextImage = UIImage(icon: FontType.fontAwesome(.forward), size: PlayerViewController.smallerButtonSize, textColor: ColorPalatte.Grey.fog, backgroundColor: UIColor.clear)
  let previousImage = UIImage(icon: FontType.fontAwesome(.backward), size: PlayerViewController.smallerButtonSize, textColor: ColorPalatte.Grey.fog, backgroundColor: UIColor.clear)
  let shuffleImage = UIImage(icon: FontType.fontAwesome(.random), size: PlayerViewController.smallerButtonSize, textColor: ColorPalatte.Grey.fog, backgroundColor: UIColor.clear)

  var trackPlaying: StoredTrack? = nil {
    didSet {
      if let track = trackPlaying {
        artistLabel?.text = track.artist
        songTitleLabel?.text = track.title
        playSongButton.setImage(pauseImage, for: .normal)
        trackDurationLabel?.text = track.formatDuration
        trackPositionLabel?.text = "0:00"
        songProgressView?.maximumValue = Float(track.time)
        songProgressView?.minimumValue = Float(0)
        DispatchQueue.main.async {
          UIApplication.shared.beginReceivingRemoteControlEvents()
          self.audioManager.resume()
        }
      }
    }
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    songTitleLabel?.text = "Select a song to start listening"
    artistLabel?.setEmptyText()
    trackDurationLabel?.setEmptyText()
    trackPositionLabel?.setEmptyText()
    playSongButton?.setImage(playImage, for: .normal)
    nextSongButton?.setImage(nextImage, for: .normal)
    previousSongButton?.setImage(previousImage, for: .normal)
    nextSongButton?.isEnabled = false
    previousSongButton?.isEnabled = false
    audioManager.delegate = self
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.becomeFirstResponder()
  }

}

extension PlayerViewController: AudioPlaybackDelegate {

  func nowPlaying(trackID: TrackID) {
    DispatchQueue.main.async { [weak self] in
      let realm = try! Realm()
      if let track = realm.object(ofType: StoredTrack.self, forPrimaryKey: trackID) {
        let unmanagedTrack = StoredTrack(value: track, schema: RLMSchema.partialShared())
        self?.trackPlaying = unmanagedTrack
      }
    }
  }

  func artworkReady(image: UIImage) {
    DispatchQueue.main.async { [weak self] in
      self?.trackArtworkImageView.image = image
    }
  }

  func updateTrackPosition(withValue value: Float, andTime time: String) {
    DispatchQueue.main.async { [weak self] in
      guard let `self` = self else { return }
      self.songProgressView.setValue(value, animated: true)
      self.trackPositionLabel?.text = time
      self.previousSongButton?.isEnabled = self.playlist.hasPrevious
      self.nextSongButton?.isEnabled = self.playlist.hasNext
    }
  }

}

extension PlayerViewController: AudioPlaybackRemoteDelegate {

  func seekSongPositionRemoteNotification(position: Float) {
    self.songProgressView?.setValue(position, animated: true)
    self.trackPositionLabel?.text = "\(position.minutes):\(position.secondsString)"
  }

  func pausedAudioRemoteNotification() {
    pauseAudio()
  }

  func resumedAudioRemoteNotification() {
    playOrResumeAudio()
  }
}
