//
//  TrackQueueModule.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/17/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation
import ReSwift

struct TrackQueuePlayNow: Action {
  var track: StoredTrack
}

struct TrackQueueEnqueNext: Action {
  var track: StoredTrack
}

struct TrackQueueAddToQueue: Action {
  var track: StoredTrack
}

struct TrackQueuePreviousTrack: Action {

}

struct TrackQueueSongFinished: Action {

}

func trackQueueReducer(_ action: Action, state: TrackQueueState?) -> TrackQueueState {
  var state = state ?? TrackQueueState(queue: TrackQueue<String>())

  switch action {
  case let action as TrackQueuePlayNow:
    state.queue = TrackQueue(track: action.track.id)
  case let action as TrackQueueEnqueNext:
    state.queue.enqueueNext(element: action.track.id)
  case let action as TrackQueueAddToQueue:
    state.queue.enqueue(action.track.id)
  case _ as TrackQueuePreviousTrack where state.queue.hasPrevious:
    state.queue.previousItem()
  case _ as TrackQueueSongFinished where state.queue.hasNext:
    state.queue.dequeue()
  default:
    break
  }
  print(state.queue)
  return state
}
