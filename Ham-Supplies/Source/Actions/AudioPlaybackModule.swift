//
//  AudioPlaybackModule.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/17/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation
import ReSwift

struct AudioSetPlayback: Action {
  var isPlaying: Bool
}

struct AudioSetCurrentTrack: Action {
  var currentTrack: StoredTrack?
}

func audioPlaybackReducer(_ action: Action, state: AudioPlaybackState?) -> AudioPlaybackState {
  var state = state ?? AudioPlaybackState(isPlaying: false, currentTrack: nil)

  switch action {
  case let action as AudioSetPlayback:
    state.isPlaying = action.isPlaying
  case let action as AudioSetCurrentTrack:
    state.currentTrack = action.currentTrack
  default:
    break
  }
  return state
}
