//
//  NetworkManager.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 4/17/18.
//  Copyright © 2018 Mike Sabo. All rights reserved.
//

import Foundation
import Disk
import AVFoundation

enum Result<String> {
  case success
  case failure(String)
}

struct NetworkManager {
  
  static func downloadLatestLibrary(completion:  @escaping (_ success: Bool, _ latestLibraryLocalURL: URL?) -> Void) {
    let sessionConfig = URLSessionConfiguration.default
    let session = URLSession(configuration: sessionConfig)
    guard let remoteURL = Router.latestLibraryRemoteURL else {
      completion(false, nil)
      return
    }
    let request = URLRequest(url: remoteURL)
    let task = session.dataTask(with: request) { (data, response, error) in
      if let libraryData = data, error == nil,
        let statusCode = (response as? HTTPURLResponse)?.statusCode,
        statusCode < 300 {
        // Success
        print("Successfully downloaded. Status code: \(statusCode)")
        if let latestURL = Router.latestLibraryLocalURL {
          do {
            try libraryData.write(to: latestURL, options: .atomic)
            completion(true, latestURL)
            return
          } catch {
            print("Failed to write latest to caches directory")
          }
        }
      }
      completion(false, nil)
      return
    }
    task.resume()
  }
  
  static func downloadSong(forTrackId trackId: TrackID, offlineFileName: String, remoteURL: URL, completion: @escaping (_ success: Bool, _ fileName: String, _ folder: Disk.Directory) -> Void) {
    
    let sessionConfig = URLSessionConfiguration.default
    let session = URLSession(configuration: sessionConfig)
    let request = URLRequest(url: remoteURL)
    print(remoteURL.absoluteString)
    
    let task = session.dataTask(with: request) { (data, response, error) in
      if let trackData = data, error == nil {
        // Success
        if let statusCode = (response as? HTTPURLResponse)?.statusCode {
          print("Successfully downloaded. Status code: \(statusCode)")
        }
        do {
          try Disk.save(trackData, to: .temporary, as: offlineFileName)
          
        } catch (let writeError) {
          print("Error creating a file \(offlineFileName) : \(writeError)")
          completion(false, offlineFileName, Disk.Directory.temporary)
          return
        }
        completion(true, offlineFileName, Disk.Directory.temporary)
      } else {
        print("Error took place while downloading a file. Error description: %@", error?.localizedDescription as Any)
        completion(false, offlineFileName, Disk.Directory.temporary)
      }
    }
    task.resume()
    
  }
  
  static func compressAndSave(trackID: TrackID, fromLocalURL local: URL, shouldDeleteTemp: Bool = false) {
    if let audioConverter = AVAudioFileConverter(inputFileURL: local, fileName: trackID) {
      audioConverter.convert(completion: { (complete, outputURL) in
        if complete {
          print("Removing \(local) from temporary")
          LocalNotification.shared.post(key: .trackSavedOffline, withInfo: ["trackID": trackID])
          var resourceValues = URLResourceValues()
          resourceValues.isExcludedFromBackup = true
          var convertedURL = outputURL
          try? convertedURL.setResourceValues(resourceValues)
          
          if shouldDeleteTemp {
            DispatchQueue.global(qos: .userInitiated).asyncAfter(deadline: (DispatchTime.now() + 3), execute: {
              try? FileManager.default.removeItem(at: local)
            })
          }
        }
      })
      
    }
  }
  
  static func downloadAndSaveSongOffline(forTrackId trackId: TrackID, offlineFileName: String, remoteURL: URL) {
    let sessionConfig = URLSessionConfiguration.default
    let session = URLSession(configuration: sessionConfig)
    let request = URLRequest(url: remoteURL)
    let task = session.dataTask(with: request) { (data, response, error) in
      if let trackData = data, error == nil {
        if let statusCode = (response as? HTTPURLResponse)?.statusCode {
          // log.debug("Successfully downloaded. Status code: \(statusCode)")
        }
        do {
          try Disk.save(trackData, to: .temporary, as: offlineFileName)
          let fileURL = try Disk.getURL(for: offlineFileName, in: Disk.Directory.temporary)
          self.compressAndSave(trackID: trackId, fromLocalURL: fileURL, shouldDeleteTemp: true)
        } catch (let writeError) {
          // log.debug("Error creating a file \(offlineFileName) : \(writeError)")
          return
        }
      }
    }
    task.resume()
  }
  
  static func downloadArtwork(forTrackId trackId: TrackID, savedFileName: String, remoteURL: URL, completion: @escaping (_ success: Bool, _ file: String) -> Void) {
    // log.info(remoteURL.absoluteString)
    let sessionConfig = URLSessionConfiguration.default
    let session = URLSession(configuration: sessionConfig)
    let request = URLRequest(url: remoteURL)
    let task = session.dataTask(with: request) { (data, response, error) in
      if let trackImage = data, error == nil,
        let artImage = UIImage(data: trackImage) {
        // Success
        if let statusCode = (response as? HTTPURLResponse)?.statusCode {
          print("Successfully downloaded. Status code: \(statusCode)")
        }
        do {
          try Disk.save(artImage, to: .temporary, as: savedFileName)
          completion(true, savedFileName)
        } catch (let writeError) {
          print("Error creating a file \(savedFileName) : \(writeError)")
          completion(false, savedFileName)
        }
        
      } else {
        print("Error took place while downloading a file. Error description: %@", error?.localizedDescription as Any)
        completion(false, savedFileName)
      }
    }
    task.resume()
    
  }
  
}
