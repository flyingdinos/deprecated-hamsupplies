//
//  HTTPMethod.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 4/17/18.
//  Copyright © 2018 Mike Sabo. All rights reserved.
//

import Foundation

public enum HTTPMethod : String {
  case get     = "GET"
  case post    = "POST"
  case put     = "PUT"
  case patch   = "PATCH"
  case delete  = "DELETE"
}
