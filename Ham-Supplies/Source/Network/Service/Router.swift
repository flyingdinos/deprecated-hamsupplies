//
//  Router.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/8/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation
import Disk
import AVFoundation

struct Router {

  static let host = "http://ham.supplies"

  static func buildURL(forPath path: String) -> URL? {
    guard let path = path.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed),
      let url = URL(string: "\(host)/\(path)") else {
        return nil
    }
    return url
  }

  static var latestLibraryRemoteURL: URL? {
    guard let latest = URL(string: host + "/library/library.latest.json") else {
      return nil
    }
    return latest
  }

  static var latestLibraryLocalURL: URL? {
    
    let cacheProvider: URL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first ?? URL(fileURLWithPath: "")
    //LocalFileProvider(for: .cachesDirectory, in: .userDomainMask)
    guard let latestLocalURL = cacheProvider.baseURL?.appendingPathComponent(Router.latestFileName) else {
      return nil
    }
    return latestLocalURL
  }

  static var latestFileName: String {
    return "latest.json"
  }
}
