//
//  AudioPlayer.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/8/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation
import Disk
import AVKit
import AVFoundation
import RxSwift
import MediaPlayer
import RealmSwift

protocol AudioPlaybackDelegate: class {
  func nowPlaying(trackID: TrackID)
  func artworkReady(image: UIImage)
  func updateTrackPosition(withValue value: Float, andTime time: String)
}

protocol AudioPlaybackRemoteDelegate: class {
  func seekSongPositionRemoteNotification(position: Float)
  func pausedAudioRemoteNotification()
  func resumedAudioRemoteNotification()
}

let mediaInfoCenter = MPNowPlayingInfoCenter.default()
let avAudioSession = AVAudioSession.sharedInstance()

class AudioPlayer {

  static var instance = AudioPlayer()
  var nowPlayingInfo: [String: Any] = [:]
  var currentAsset: AVAsset!
  var preparingNextTrack = false
  var assetPlayer: AVPlayer = AVPlayer()
  var disposeBag: DisposeBag! = DisposeBag()
  weak var delegate: AudioPlaybackDelegate?

  var playerBoundaryObserver: Any?
  var prepareNextTrackObserver: Any?
  var updatePlayCountObsever: Any?
  var updateTrackPosition: Any?

  let assetKeys = ["playable"]
  let mediaKeys = ["artist", "title", "type", "albumName", "artwork"]

  init() {
    assetPlayer = AVPlayer()
    TrackQueueBus.shared
      .playingObserver()
      .subscribe(onNext: { nextTrack in
        self.prepareToPlay(trackId: nextTrack)
      }).disposed(by: disposeBag)

    remoteCommandCenter.playCommand.addTarget(handler: handlePlayCommandEvent(_:))
    remoteCommandCenter.pauseCommand.addTarget(handler: handlePauseCommandEvent(_:))
    remoteCommandCenter.togglePlayPauseCommand.addTarget(handler: handleTogglePlayPauseCommandEvent(_:))
    remoteCommandCenter.nextTrackCommand.addTarget(handler: handleNextTrackCommandEvent(_:))
    remoteCommandCenter.previousTrackCommand.addTarget(handler: handlePreviousTrackCommandEvent(_:))
    remoteCommandCenter.changePlaybackPositionCommand.addTarget(handler: handleChangePlaybackPositionCommandEvent(_:))
    do {
      try avAudioSession.setCategory(AVAudioSessionCategoryPlayback)
      try avAudioSession.setActive(true)
    } catch {
      print("COULD NOT START AVAUDIOSESSION")
    }
  }

  func setMPNowPlayingMetaData() {
    mediaInfoCenter.playbackState = .playing
    mediaInfoCenter.nowPlayingInfo = self.nowPlayingInfo
    remoteCommandCenter.playCommand.isEnabled = true
    remoteCommandCenter.pauseCommand.isEnabled = true
    remoteCommandCenter.stopCommand.isEnabled = true
    remoteCommandCenter.togglePlayPauseCommand.isEnabled = true
    remoteCommandCenter.nextTrackCommand.isEnabled = true
    remoteCommandCenter.previousTrackCommand.isEnabled = true
    remoteCommandCenter.changePlaybackRateCommand.isEnabled = true
  }

  func play(fromLocalURL localURL: URL, trackID: TrackID) {
    let realm = try! Realm()
    guard let track = realm.object(ofType: StoredTrack.self, forPrimaryKey: trackID) else {
      return
    }
    nowPlayingInfo = [:]
    print("Removing Player Boundary")
    self.remove(assetObserver: playerBoundaryObserver)
    print("Removing Next Track Observer")
    self.remove(assetObserver: prepareNextTrackObserver)
    print("Removing Update Play Count Observer")
    self.remove(assetObserver: updatePlayCountObsever)
    print("Removing Track Duration Observer")
    self.remove(assetObserver: updateTrackPosition)

    currentAsset = AVAsset(url: localURL)
    let item = AVPlayerItem(asset: currentAsset, automaticallyLoadedAssetKeys: assetKeys)
    assetPlayer.replaceCurrentItem(with: nil)
    nowPlayingInfo = retrieveItemMetadata(forTrack: track)
    assetPlayer.replaceCurrentItem(with: item)
    setMPNowPlayingMetaData()
    preparingNextTrack = false
    let trackOverTime = CMTimeMakeWithSeconds(Float64(currentAsset.duration.f - 1), 1)
    let trackOverValue = NSValue(time: trackOverTime)
    playerBoundaryObserver = assetPlayer.addBoundaryTimeObserver(forTimes: [trackOverValue], queue: DispatchQueue.main, using: {
      _ = TrackQueueBus.shared.nextSong()
    })

    prepareNextTrackObserver = assetPlayer.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(20, 1), queue: DispatchQueue.global(qos: .utility), using: { [weak self] time in
      guard let `self` = self, !self.preparingNextTrack else { return }
      let currentTime = time.f
      let assetDuration = self.currentAsset.duration.f
      guard currentTime > (assetDuration / 8) else { return }
      if let nextTrackID = TrackQueueBus.shared.peekNextTrackID {
        print("Downloading Next Queued Track")
        self.preparingNextTrack = true
        self.downloadNextQueuedTrack(withTrackID: nextTrackID)
      }
    })

    updatePlayCountObsever = assetPlayer.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(20, 1), queue: DispatchQueue.global(qos: .utility), using: { [weak self] time in
      guard let `self` = self else { return }
      let currentTime = time.f
      let assetDuration = self.currentAsset.duration.f
      guard currentTime > (assetDuration / 2), currentTime > (assetDuration * 0.5) else { return }
      if let currentTrack = TrackQueueBus.shared.currentTrack {
        StoredTrack.updatePlayCount(forTrackID: currentTrack)
        self.remove(assetObserver: self.updatePlayCountObsever)
        self.updatePlayCountObsever = nil
      }
    })

    updateTrackPosition = assetPlayer.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, 1), queue: DispatchQueue.global(qos: .userInteractive), using: { [weak self] time in
      guard let `self` = self else { return }
      if self.isPlaying {
        let trackProgressValue = self.assetPlayer.currentTime().f
        let time = "\(trackProgressValue.minutes):\(trackProgressValue.secondsString)"
        self.delegate?.updateTrackPosition(withValue: trackProgressValue, andTime: time)
        self.nowPlayingInfo[MPNowPlayingInfoPropertyElapsedPlaybackTime] = NSNumber(value: Double(trackProgressValue))
        self.setMPNowPlayingMetaData()
      }
    })
    delegate?.nowPlaying(trackID: track.id)
  }

  func remove(assetObserver: Any?) {
    guard let observer = assetObserver else { return }
    assetPlayer.removeTimeObserver(observer)
  }

  func seek(toTime value: Float) {
    let seconds: Int64 = Int64(value)
    let targetTime: CMTime = CMTimeMake(seconds, 1)
    assetPlayer.seek(to: targetTime)
    nowPlayingInfo[MPNowPlayingInfoPropertyElapsedPlaybackTime] = NSNumber(value: value)
    setMPNowPlayingMetaData()
    if isPlaying {
      assetPlayer.play()
    }
  }

  func getArtwork(for track: StoredTrack) {
    do {
      let image = try Disk.retrieve(fromURL: track.artworkDiskResults.fileURL, as: UIImage.self)
      self.delegate?.artworkReady(image: image)
    } catch {
      print("Failed to retrieve artwork from path \(track.artworkDiskResults.fileURL) in the \(track.artworkDiskResults.folder.path) folder")
    }
  }

  func prepareToPlay(trackId: TrackID) {
    let realm = try! Realm()
    guard let track = realm.object(ofType: StoredTrack.self, forPrimaryKey: trackId) else {
      return
    }
    let searchResults = Disk.search(forFile: track.offlineFileName, convertedFileName: track.convertedFileName)
    if searchResults.found {
      print("Found track localy")
      print(searchResults.fileURL)
      play(fromLocalURL: searchResults.fileURL, trackID: trackId)
    } else {
      downloadSong(forTrack: track)
    }
  }

  func downloadArtwork(forTrack track: StoredTrack) {
    print("DOWNLOADING ARTWORK FOR TRACK")
    guard let artworkURL = track.artworkRemoteURL else {
      return
    }
    NetworkManager.downloadArtwork(forTrackId: track.id, savedFileName: track.offlineArtworkFileName, remoteURL: artworkURL, completion: { (_, savedImage) in
      // log.debug("RETRIEVED ARTWORK FOR TRACK")
      do {
        let image = try Disk.retrieve(savedImage, from: .temporary, as: UIImage.self)
        self.nowPlayingInfo[MPMediaItemPropertyArtwork] = image.createMPMediaArtwork()
        self.setMPNowPlayingMetaData()
        // log.debug("Artwork ready delegate being called")
        self.delegate?.artworkReady(image: image)
      } catch {
        // log.debug("Could not retrieve image artwork")
      }
    })
  }

  func downloadNextQueuedTrack(withTrackID trackID: TrackID) {
    let realm = try! Realm()
    guard let track = realm.object(ofType: StoredTrack.self, forPrimaryKey: trackID),
          let url = track.songRemoteURL else {
      return
    }
    let searchResults = Disk.search(forFile: track.offlineFileName, convertedFileName: track.convertedFileName)
    if searchResults.found {
      print("Song exists Locally")
      self.remove(assetObserver: self.prepareNextTrackObserver)
      self.prepareNextTrackObserver = nil
      return
    }
    NetworkManager.downloadSong(forTrackId: trackID, offlineFileName: track.offlineFileName, remoteURL: url, completion: { [weak self] success, _, _ in
      if success {
        self?.remove(assetObserver: self?.prepareNextTrackObserver)
        self?.prepareNextTrackObserver = nil
        print("Next song downloaded successfully")
      }
    })
  }

  func downloadSong(forTrack track: StoredTrack) {
    guard let url = track.songRemoteURL else {
      return
    }
    let trackID = track.id
    NetworkManager.downloadSong(forTrackId: trackID, offlineFileName: track.offlineFileName, remoteURL: url, completion: { success, file, directory in
      if success {
        do {
        let fileURL = try Disk.getURL(for: file, in: directory)
          self.play(fromLocalURL: fileURL, trackID: trackID)
        } catch {
          // log.debug("ERROR getting FILE URL for \(trackID)")
        }
      } else {
        // log.debug("ERROR downloading song")
      }
    })
  }

  func selected(track: StoredTrack) {
    TrackQueueBus.shared.playTrack(track.id)
  }

  func pause() {
    assetPlayer.pause()
    nowPlayingInfo[MPNowPlayingInfoPropertyPlaybackRate] = 0.0
    mediaInfoCenter.nowPlayingInfo = self.nowPlayingInfo
    mediaInfoCenter.playbackState = .paused
  }

  func resume() {
    assetPlayer.play()
    nowPlayingInfo[MPNowPlayingInfoPropertyPlaybackRate] = 1.0
    mediaInfoCenter.nowPlayingInfo = nowPlayingInfo
    mediaInfoCenter.playbackState = .playing
  }

  var isPlaying: Bool {
    return assetPlayer.isPlaying
  }

  @objc func handlePauseCommandEvent(_ event: MPRemoteCommandEvent) -> MPRemoteCommandHandlerStatus {
    pause()
    return .success
  }

  @objc func handlePlayCommandEvent(_ event: MPRemoteCommandEvent) -> MPRemoteCommandHandlerStatus {
    if assetPlayer.currentItem != nil {
      resume()
    }
    return .success
  }

  @objc func handleStopCommandEvent(_ event: MPRemoteCommandEvent) -> MPRemoteCommandHandlerStatus {
    pause()
    return .success
  }

  @objc func handleTogglePlayPauseCommandEvent(_ event: MPRemoteCommandEvent) -> MPRemoteCommandHandlerStatus {
    isPlaying ? pause() : resume()
    return .success
  }

  // MARK: Track Changing Command Handlers
  @objc func handleNextTrackCommandEvent(_ event: MPRemoteCommandEvent) -> MPRemoteCommandHandlerStatus {
    if TrackQueueBus.shared.hasNext {
      TrackQueueBus.shared.nextSong()
      return .success
    } else {
      return .noSuchContent
    }
  }

  @objc func handlePreviousTrackCommandEvent(_ event: MPRemoteCommandEvent) -> MPRemoteCommandHandlerStatus {
    if TrackQueueBus.shared.hasPrevious {
      TrackQueueBus.shared.playPreviousSong()
      return .success
    } else {
      return .noSuchContent
    }
  }

  @objc func handleChangePlaybackPositionCommandEvent(_ event: MPRemoteCommandEvent) -> MPRemoteCommandHandlerStatus {
    if let playbackCommandEvent = event as? MPChangePlaybackPositionCommandEvent,
      currentAsset != nil {
      seek(toTime: Float(playbackCommandEvent.positionTime))
      return .success
    } else {
      return .commandFailed
    }
  }

}

extension AVPlayer {
  var isPlaying: Bool {
    return rate != 0 && error == nil
  }
}

extension UIImage {

  func createMPMediaArtwork() -> MPMediaItemArtwork {
    let artwork = MPMediaItemArtwork(boundsSize: CGSize.zero, requestHandler: { (_) -> UIImage in
      return self
    })
    return artwork
  }
}

extension AudioPlayer {

  func retrieveItemMetadata(forTrack track: StoredTrack) -> [String: Any] {
    var dict: [String: Any] = [:]
    var createdArtwork = false
    currentAsset.metadata.forEach({ (mediaItem)  in
      if mediaItem.commonKey?.rawValue == "artwork" {
        if let artData = mediaItem.dataValue, artData.count > 0 {
          DispatchQueue.main.async {

            if let image = UIImage(data: artData) {
              // log.debug("PUBLISHING IMAGE TO DELEGATE")
              dict[MPMediaItemPropertyArtwork] = image.createMPMediaArtwork()
              createdArtwork = true
              self.delegate?.artworkReady(image: image)
  //            TrackQueueBus.shared.publish(trackImage: image)
            }
          }
        }
      }
    })
    if !createdArtwork {
      // log.debug("Downloading ARtwork not found")
      downloadArtwork(forTrack: track)
    }

    let duration = NSNumber(value: track.time)
    dict[MPMediaItemPropertyArtist] = track.artist
    dict[MPMediaItemPropertyAlbumTitle] = track.album
    dict[MPMediaItemPropertyGenre] = track.genre
    dict[MPMediaItemPropertyTitle] = track.title
    dict[MPMediaItemPropertyAssetURL] = track.songDiskResults.fileURL
    dict[MPMediaItemPropertyPlaybackDuration] = duration
    dict[MPNowPlayingInfoPropertyElapsedPlaybackTime] = 0.0
    dict[MPMediaItemPropertyMediaType] = 1
    dict[MPNowPlayingInfoPropertyPlaybackRate] = currentAsset.preferredRate

    return dict
  }

}

//guard let key = mediaItem.commonKey?.rawValue else { return  }
//switch key {
//case "artist":
//
//case "albumName":
//
//case "type":
//
//case "title":
//
//default:
//  break
//}
//extension AudioPlayer: AVAudioPlayerDelegate {
//
//  func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
//
//  }
//
//}
//
