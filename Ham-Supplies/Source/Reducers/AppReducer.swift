//
//  AppReducer.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/17/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation
import ReSwift

func appReducer(action: Action, state: AppState?) -> AppState {
  return AppState(
    trackQueue: trackQueueReducer(action, state: state?.trackQueue),
    audioPlayback: audioPlaybackReducer(action, state: state?.audioPlayback)
  )
}
