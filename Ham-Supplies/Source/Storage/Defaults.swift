//
//  Defaults.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/12/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

protocol UserDefaultsProtocol: Any {
//  var firstLaunch: Bool { get set }
//  var compressTracksForStorage: Bool { get set }
  var shufflePlaylist: Bool { get set }
  var repteatPlaylist: Bool { get set }
  var requireWifiOnly: Bool { get set }
  var continueUnplayedTracks: Bool { get set }
  var lastUpdatedLibrary: String { get set }
//  var offlineTrackBitRate: Int { get set }

}

struct UserDefaultKeys {
  static let compressTracksForStorage = "compressForOffline"
  static let shufflePlaylist = "shufflePlaylist"
  static let repeatPlaylist = "repteatPlaylist"
  static let requireWifiOnly = "requireWifiOnly"
  static let continueUnplayedTracks = "continueUnplayedTracks"
  static let lastUpdatedLibrary = "lastUpdatedLibrary"
  static let offlineTrackBitRate = "offlineTrackBitRate"
  static let firstLaunch = "firstLaunch"
}

extension DefaultsKeys {
  static let compressTracksForStorage = DefaultsKey<Bool>(UserDefaultKeys.compressTracksForStorage)
  static let shufflePlaylist = DefaultsKey<Bool>(UserDefaultKeys.shufflePlaylist)
  static let repeatPlaylist = DefaultsKey<Bool>(UserDefaultKeys.repeatPlaylist)
  static let requireWifiOnly = DefaultsKey<Bool>(UserDefaultKeys.requireWifiOnly)
  static let continueUnplayedTracks = DefaultsKey<Bool>(UserDefaultKeys.continueUnplayedTracks)
  static let lastUpdatedLibrary = DefaultsKey<String?>(UserDefaultKeys.lastUpdatedLibrary)
  static let offlineTrackBitRate = DefaultsKey<Int>(UserDefaultKeys.offlineTrackBitRate)
  static let firstLaunch = DefaultsKey<Bool>(UserDefaultKeys.firstLaunch)
}

extension UserDefaults {
  func registerUserDefaults() {
    if let lastUpdated = Defaults[.lastUpdatedLibrary], lastUpdated.count > 0 {
      return
    }
    Defaults[.lastUpdatedLibrary] = ""
    Defaults[.compressTracksForStorage] = true
    Defaults[.shufflePlaylist] = false
    Defaults[.repeatPlaylist] = false
    Defaults[.requireWifiOnly] = true
    Defaults[.continueUnplayedTracks] = false
    Defaults[.offlineTrackBitRate] = 128000
    Defaults[.firstLaunch] = false

  }
}

//var Defaults = DefaultKeys
//
//final class Defaults: UserDefaultsProtocol {
//
//  static let instance = Defaults()
//  let userDefaults = UserDefaults.standard
//  private init() { }
//
//  func registerUserDefaults() {
//    let dictionary: [String: Any] = [UserDefaultKeys.compressTracksForStorage: true,
//                                     UserDefaultKeys.shufflePlaylist: false,
//                                     UserDefaultKeys.repteatPlaylist: false,
//                                     UserDefaultKeys.requireWifiOnly: true,
//                                     UserDefaultKeys.continueUnplayedTracks: false]
//
//    UserDefaults.standard.register(defaults: dictionary)
//    UserDefaults.standard.synchronize()
//  }
//
//  var compressTracksForStorage: Bool {
//    get {
//      return userDefaults.bool(forKey: UserDefaultKeys.compressTracksForStorage)
//    } set {
//      userDefaults.set(newValue, forKey: UserDefaultKeys.compressTracksForStorage)
//    }
//  }
//
//  var shufflePlaylist: Bool {
//    get {
//      return userDefaults.bool(forKey: UserDefaultKeys.shufflePlaylist)
//    } set {
//      userDefaults.set(newValue, forKey: UserDefaultKeys.shufflePlaylist)
//    }
//  }
//
//  var repteatPlaylist: Bool {
//    get {
//      return userDefaults.bool(forKey: UserDefaultKeys.repteatPlaylist)
//    } set {
//      userDefaults.set(newValue, forKey: UserDefaultKeys.repteatPlaylist)
//    }
//  }
//
//  var requireWifiOnly: Bool {
//    get {
//      return userDefaults.bool(forKey: UserDefaultKeys.requireWifiOnly)
//    } set {
//      userDefaults.set(newValue, forKey: UserDefaultKeys.requireWifiOnly)
//    }
//  }
//
//  var continueUnplayedTracks: Bool {
//    get {
//      return userDefaults.bool(forKey: UserDefaultKeys.continueUnplayedTracks)
//    } set {
//      userDefaults.set(newValue, forKey: UserDefaultKeys.continueUnplayedTracks)
//    }
//  }
//
//  var lastUpdatedLibrary: String {
//    get {
//      return userDefaults.string(forKey: UserDefaultKeys.lastUpdatedLibrary) ?? ""
//    } set {
//      userDefaults.set(newValue, forKey: UserDefaultKeys.lastUpdatedLibrary)
//    }
//  }
//
//}
//
//final class MockDefaults: UserDefaultsProtocol {
//
//  init() { }
//
//  var _compressOffline: Bool = true
//  var compressTracksForStorage: Bool {
//    get {
//      return _compressOffline
//    } set {
//      _compressOffline = newValue
//    }
//  }
//
//  var testShufflePlaylist: Bool = false
//  var shufflePlaylist: Bool {
//    get {
//      return testShufflePlaylist
//    } set {
//      testShufflePlaylist = newValue
//    }
//  }
//
//  var testRepteatPlaylist = false
//  var repteatPlaylist: Bool {
//      get {
//        return testRepteatPlaylist
//      } set {
//        testRepteatPlaylist = newValue
//      }
//    }
//
//  var testRequireWifiOnly = true
//  var requireWifiOnly: Bool {
//    get {
//      return testRequireWifiOnly
//    } set {
//      testRequireWifiOnly = newValue
//    }
//  }
//
//  var testContinuedUnplayedTracks = false
//  var continueUnplayedTracks: Bool {
//    get {
//      return testContinuedUnplayedTracks
//    } set {
//      testContinuedUnplayedTracks = newValue
//    }
//  }
//
//  var testLastUpdatedLibrary: String = ""
//  var lastUpdatedLibrary: String {
//    get {
//      return testLastUpdatedLibrary
//    } set {
//      testLastUpdatedLibrary = newValue
//    }
//  }
////
//}
//
//var dateEncoder: JSONEncoder {
//  let encoder = JSONEncoder()
//  encoder.dateEncodingStrategy = .formatted(utcDateFormatter)
//  return encoder
//}
//
//var dateDecoder: JSONDecoder {
//  let decoder = JSONDecoder()
//  decoder.dateDecodingStrategy = .formatted(utcDateFormatter)
//  return decoder
//}
