//
//  Directory.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/22/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation

extension FileManager {

  func createOfflineTrackCacheDirectories() {
    var isTrackDir: ObjCBool = true
    var isArtworkDir: ObjCBool = true

    do {
      let documentsURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
      let savedTrackDirectory = documentsURL.appendingPathComponent(Folder.savedTracks.path)
      let savedArtworkDirectory = documentsURL.appendingPathComponent(Folder.savedArtwork.path)

      if !FileManager.default.fileExists(atPath: savedTrackDirectory.path, isDirectory: &isTrackDir) {
        try FileManager.default.createDirectory(at: savedTrackDirectory, withIntermediateDirectories: true, attributes: nil)
      }

      if !FileManager.default.fileExists(atPath: savedArtworkDirectory.path, isDirectory: &isArtworkDir) {
        try FileManager.default.createDirectory(at: savedArtworkDirectory, withIntermediateDirectories: true, attributes: nil)
      }

    } catch {

    }
  }
}
