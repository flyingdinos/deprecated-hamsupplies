//
//  RealmDataStore.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/22/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

protocol RealmConfigurable: class {
  var fileName: String { get }
  var fileURL: URL { get }
  var inMemory: Bool { get }
  var configuration: Realm.Configuration { get }
//  var instance: Realm { get }
}

extension RealmConfigurable {
  var fm: FileManager {
    return FileManager.default
  }
}

final class TemporaryTrackStorage: RealmConfigurable {

  let fileName: String = "inMemoryTrackRealm.realm"
  var inMemory: Bool = true
  var fileURL: URL {
    return URL(fileURLWithPath: NSTemporaryDirectory() + fileName)
  }
  var configuration: Realm.Configuration {
    print("IN TEMP CONFIG")
    return Realm.Configuration(inMemoryIdentifier: fileName)
  }

//  var instance: Realm {
//    return try! Realm(configuration: configuration)
//  }

}

final class DefaultRealmStore: RealmConfigurable {

  let fileName = "ham-supplies.realm"
  let inMemory: Bool = false

  var fileURL: URL {
    let url = URL(fileURLWithPath: RLMRealmPathForFile(fileName))
    return url
  }

  var configuration: Realm.Configuration {
    print("IN DEFAULT CONFIG")
    let config = Realm.Configuration(fileURL: fileURL)
    Realm.Configuration.defaultConfiguration = config
    return config
  }

}
