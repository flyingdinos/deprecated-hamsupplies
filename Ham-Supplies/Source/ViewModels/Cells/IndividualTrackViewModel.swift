//
//  IndividualTrackViewModel.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/27/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation
import IGListKit

final class IndividualTrackViewModel {

  var track: Track
  var index: Int = -1

  init(track: Track, itemIndex: Int) {
    self.track = track
    index = itemIndex
  }

}

extension IndividualTrackViewModel: ListDiffable {

  func diffIdentifier() -> NSObjectProtocol {
    return track.diffIdentifier()
  }

  func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
    if self === object { return true }
    guard let object = object as? IndividualTrackViewModel else { return false }
    return object.track.id == track.id && index == object.index
  }

}

final class StoredTrackViewModel {

  var track: StoredTrack
  var index: Int = -1

  init(track: StoredTrack, itemIndex: Int) {
    self.track = track
    index = itemIndex
    print(track.title)
  }

}

extension StoredTrackViewModel: ListDiffable {

  func diffIdentifier() -> NSObjectProtocol {
    return track.diffIdentifier()
  }

  func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
    if self === object { return true }
    guard let object = object as? StoredTrackViewModel else { return false }
    return object.track.id == track.id && index == object.index
  }

}
