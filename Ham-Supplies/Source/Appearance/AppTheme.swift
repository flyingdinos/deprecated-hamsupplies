//
//  AppTheme.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/20/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation
import DynamicColor

struct AppTheme {

  static var backgroundColor: UIColor = ColorPalatte.Blue.moonlight

//  static var backgroundColor: UIColor = UIColor(hexString: "#161f28")
//  static var backgroundColor: UIColor = UIColor(hexString: "#161f28")
}

struct ColorPalatte {

  struct Blue {
    static var wetAsphalt: UIColor = UIColor(hexString: "#34495e")
    static var midnightBlue: UIColor = UIColor(hexString: "#2c3e50")
    static var twilight: UIColor = UIColor(hexString: "#22313d")
    static var moonlight: UIColor = UIColor(hexString: "#161f28")
    static var darkness: UIColor = UIColor(hexString: "#0c1216")
  }

  struct Grey {
    static var clouds: UIColor = UIColor(hexString: "#ecf0f1")
    static var fog: UIColor = UIColor(hexString: "#d4d9dc")
    static var zinc: UIColor = UIColor(hexString: "#cacfd3")
    static var silver: UIColor = UIColor(hexString: "#bdc3c7")
    static var concrete: UIColor = UIColor(hexString: "#95a5a6")
    static var asbestos: UIColor = UIColor(hexString: "#7f8c8d")
  }

  struct NeonBlue {
    static var peterRiver: UIColor = UIColor(hexString: "#3498db")
    static var belizeHole: UIColor = UIColor(hexString: "#2980b9")
  }

  struct Bright {
    static var sunflower: UIColor = UIColor(hexString: "#f1c40f")
    static var orange: UIColor = UIColor(hexString: "#f39c12")
  }

  struct Red {
    static var alizarin: UIColor = UIColor(hexString: "#e74c3c")
    static var pomegranate: UIColor = UIColor(hexString: "#c0392b")
  }

}
