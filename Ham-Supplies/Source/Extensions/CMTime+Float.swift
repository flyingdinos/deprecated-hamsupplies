//
//  CMTime+Float.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/31/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import AVFoundation

extension Float {
  
  var minutes: Int {
    return Int(self / 60)
  }
  
  var seconds: Int {
    return Int(self.truncatingRemainder(dividingBy: 60))
  }
  
  var secondsString: String {
    let seconds = self.seconds
    if seconds < 10 {
      return "0" + "\(seconds)"
    } else {
      return "\(seconds)"
    }
  }
}

extension CMTime {
  var f: Float {
    return Float(self.f64)
  }
  var f64: Float64 {
    return CMTimeGetSeconds(self)
  }
}
