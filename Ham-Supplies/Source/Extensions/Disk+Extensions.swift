//
//  Disk+Extensions.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/13/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation
import Disk

typealias DiskSearchResult = (found: Bool, folder: Folder, fileURL: URL)

extension Disk {

  static func search(forFile fileName: String, convertedFileName: String = "") -> DiskSearchResult {
    if Folder.savedTracks.exists(withFileName: convertedFileName) {
      return (true, Folder.savedTracks, Folder.savedTracks.fileURL(withFileName: convertedFileName))
    } else if Folder.savedArtwork.exists(withFileName: fileName) {
      return (true, Folder.savedArtwork, Folder.savedArtwork.fileURL(withFileName: fileName))
    } else if Folder.temporary.exists(withFileName: fileName) {
      return (true, Folder.temporary, Folder.temporary.fileURL(withFileName: fileName))
    } else {
      return (false, Folder.none, URL(fileURLWithPath: NSTemporaryDirectory()))
    }
  }

  static func retrieve(fromURL url: URL, as type: Data.Type) throws -> Data {
    do {
      let data = try Data(contentsOf: url)
      return data
    } catch {
      throw error
    }
  }

  static func retrieveSongData(forTrack track: Track, inFolder folder: Folder, as type: Data.Type) throws -> Data {
    do {
      guard folder.exists(withFileName: track.offlineFileName) else {
        throw NSError(domain: Disk.errorDomain, code: Disk.ErrorCode.noFileFound.rawValue, userInfo: [:]) as Error
      }
      let trackURL = folder.fileURL(withFileName: track.offlineFileName)
      let data = try Data(contentsOf: trackURL)
      return data
    } catch {
      throw error
    }
  }

  static func retrieveArtwork(forTrack track: Track) throws -> UIImage {
    let searchResults = track.artworkDiskResults
    guard searchResults.found else { throw Disk.createError(.noFileFound, description: "Artwork not found", failureReason: "File not found", recoverySuggestion: nil) }
    let artImageURL = searchResults.fileURL

    do {
      let image = try retrieve(fromURL: artImageURL, as: UIImage.self)
      return image
    } catch {
      throw error
    }
  }

  static func retrieve(fromURL url: URL, as type: UIImage.Type) throws -> UIImage {
    do {
      let data = try Data(contentsOf: url)
      if let image = UIImage(data: data) {
        return image
      } else {
        throw createError(
          .deserialization,
          description: "Could not decode UIImage from \(url.path).",
          failureReason: "A UIImage could not be created out of the data in \(url.path).",
          recoverySuggestion: "Try deserializing \(url.path) manually after retrieving it as Data."
        )
      }
    } catch {
      throw error
    }
  }

  static func createError(_ errorCode: ErrorCode, description: String?, failureReason: String?, recoverySuggestion: String?) -> Error {
    let errorInfo: [String: Any] = [NSLocalizedDescriptionKey: description ?? "",
                                    NSLocalizedRecoverySuggestionErrorKey: recoverySuggestion ?? "",
                                    NSLocalizedFailureReasonErrorKey: failureReason ?? ""]
    return NSError(domain: errorDomain, code: errorCode.rawValue, userInfo: errorInfo) as Error
  }

}

extension Disk.Directory: Equatable {

}

public func ==(lhs: Disk.Directory, rhs: Disk.Directory) -> Bool {
  return lhs == rhs
}
