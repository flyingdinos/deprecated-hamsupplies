//
//  UIViewController+Extensions.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/8/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import UIKit

extension UIViewController {

  func preloadView() {
    _ = view
  }
}
