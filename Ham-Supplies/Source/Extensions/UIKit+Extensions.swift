//
//  UIKit+Extensions.swift
//  Ham-Supplies
//
//  Created by Mike Sabo on 10/31/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
  
  func setEmptyTitle() {
    self.titleLabel?.setEmptyText()
  }
  
}

extension UILabel {
  
  func setEmptyText() {
    self.text = ""
  }
}
