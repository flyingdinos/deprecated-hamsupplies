//
//  TrackQueueSpec.swift
//  Ham-SuppliesTests
//
//  Created by Mike Sabo on 10/24/17.
//  Copyright © 2017 Mike Sabo. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import Ham_Supplies

class TrackQueueSpec: QuickSpec {

  override func spec() {

    describe("Track Queue") {
      
      var queue: TrackQueue<String>!
      
      beforeEach {
        queue = TrackQueue<String>()
      }

      it("should allow initialization with array of objects") {
        queue = TrackQueue(tracks: ["one", "two", "three", "four", "five", "six", "seven"])
        expect(queue.count).to(equal(7))
        expect(queue.current).to(equal("one"))
      }
      
      it("hasPrevious should be false after initilization") {
        queue = TrackQueue(tracks: ["one", "two", "three", "four", "five", "six", "seven"])
        expect(queue.hasPrevious).to(beFalse())
      }
      
      it("hasNext should be true after initilization with multiple items") {
        queue = TrackQueue(tracks: ["one", "two", "three", "four", "five", "six", "seven"])
        expect(queue.hasNext).to(beTrue())
      }
      
      it("hasNext should be false after initilization with one object array") {
        queue = TrackQueue(tracks: ["one"])
        expect(queue.hasNext).to(beFalse())
      }
      
      it("hasNext should be false after initilization with one object") {
        queue = TrackQueue(track: "one")
        expect(queue.hasNext).to(beFalse())
      }
      
      it("hasPrevious should be true after one or more item's have been dequeue'd") {
        queue = TrackQueue(tracks: ["one", "two", "three", "four", "five", "six", "seven"])
        queue.dequeue()
        expect(queue.hasPrevious).to(beTrue())
        queue.dequeue()
        expect(queue.current).to(equal("three"))
        expect(queue.hasPrevious).to(beTrue())
      }
      
      it("hasNext should be true after one or more item's have been dequeue'd") {
        queue = TrackQueue(tracks: ["one", "two", "three", "four", "five", "six", "seven"])
        queue.dequeue()
        expect(queue.hasNext).to(beTrue())
        queue.dequeue()
        expect(queue.current).to(equal("three"))
        expect(queue.hasNext).to(beTrue())
      }
      
      it("should not pop to previous when hasPrevious is false") {
        queue = TrackQueue(tracks: ["one", "two", "three", "four", "five", "six", "seven"])
        expect(queue.hasPrevious).to(beFalse())
        let previousItem = queue.previousItem()
        expect(previousItem).to(beNil())
        expect(queue.current).to(equal("one"))
      }
      
      it("should not advance to next item when hasNext is false") {
        queue = TrackQueue(tracks: ["one", "two", "three"])
        for _ in 1..<queue.count { queue.dequeue() }
        expect(queue.hasNext).to(beFalse())
        queue.dequeue()
        expect(queue.current).to(equal("three"))
        expect(queue.peekNext()).to(beNil())
      }
      
      it("should update hasNext to true when a new item is enqueued after hasNext was false") {
        queue = TrackQueue(tracks: ["one", "two", "three"])
        for _ in 1..<queue.count { queue.dequeue() }
        expect(queue.hasNext).to(beFalse())
        queue.enqueue("next")
        expect(queue.current).to(equal("three"))
        expect(queue.hasNext).to(beTrue())
        expect(queue.peekNext()).to(equal("next"))
      }
      
      it("hasNext should be false after all items have been dequeue'd and remaining is 0") {
        queue = TrackQueue(tracks: ["one", "two", "three", "four", "five", "six", "seven"])
        for _ in 1..<queue.count { queue.dequeue() }
        expect(queue.hasNext).to(beFalse())
        expect(queue.current).to(equal("seven"))
        expect(queue.remaining).to(equal(0))
        expect(queue.played).to(equal(6))
        expect(queue.hasPrevious).to(beTrue())
      }
      
      it("should always remain on the last item when dequeue called more times than elements") {
        queue = TrackQueue(tracks: ["one", "two", "three", "four", "five", "six", "seven"])
        for _ in 1..<15 { queue.dequeue() }
        expect(queue.hasNext).to(beFalse())
        expect(queue.current).to(equal("seven"))
        expect(queue.remaining).to(equal(0))
        expect(queue.played).to(equal(6))
        expect(queue.hasPrevious).to(beTrue())
      }
      
      it("peekNext should be nil after all items have been dequeue'd and remaining is 0") {
        queue = TrackQueue(tracks: ["one", "two", "three", "four", "five", "six", "seven"])
        for _ in 1..<queue.count { queue.dequeue() }
        expect(queue.peekNext()).to(beNil())
      }
      
      it("pop back one item after calling previous item") {
        queue = TrackQueue(tracks: ["one", "two", "three", "four", "five", "six", "seven"])
        for _ in 1..<3 { queue.dequeue() }
        expect(queue.remaining).to(equal(4))
        let previousItem = queue.previousItem()
        expect(queue.remaining).to(equal(5))
        expect(queue.current).to(equal(previousItem))
      }
      
      it("add new items at the end of the queue") {
        queue = TrackQueue(tracks: ["one", "two", "three"])
        expect(queue.count).to(equal(3))
        for item in ["four", "five", "six", "seven"] {
          queue.enqueue(item)
        }
        expect(queue.count).to(equal(7))
        expect(queue.elements).to(equal(["one", "two", "three", "four", "five", "six", "seven"]))
      }
      
      it("allow new item to be enqueue'd next") {
        queue = TrackQueue(tracks: ["one", "two", "three"])
        queue.enqueueNext(element: "four")
        expect(queue.count).to(equal(4))
        expect(queue.elements).to(equal(["one", "four", "two", "three"]))
      }
      
      it("allow new item to be enqueue'd next, when head is greater than 0") {
        queue = TrackQueue(tracks: ["one", "two", "three"])
        queue.dequeue()
        queue.enqueueNext(element: "four")
        expect(queue.count).to(equal(4))
        expect(queue.elements).to(equal(["one", "two", "four", "three"]))
      }
      
      it("shuffle elements in queue") {
        queue = TrackQueue(tracks: ["one", "two", "three", "four", "five", "six", "seven"])
        queue.shuffle()
        expect(queue.elements).toNot(equal(["one", "two", "three", "four", "five", "six", "seven"]))
      }
      
      it("shuffle elements in queue and leave current item as head when has previous is true") {
        queue = TrackQueue(tracks: ["one", "two", "three", "four", "five", "six", "seven"])
        for _ in 1...3 { queue.dequeue() }
        expect(queue.current).to(equal("four"))
        expect(queue.first).to(equal("one"))
        expect(queue.remaining).to(equal(3))
        expect(queue.played).to(equal(3))
        expect(queue.hasPrevious).to(beTrue())
        queue.shuffle()
        expect(queue.current).to(equal("four"))
        expect(queue.first).to(equal("four"))
        expect(queue.remaining).to(equal(6))
        expect(queue.played).to(equal(0))
        expect(queue.hasPrevious).to(beFalse())
      }
      
      it("should clear the queue when play single is called") {
        queue = TrackQueue(tracks: ["one", "two", "three", "four", "five", "six", "seven"])
        queue.playSingle(track: "test")
        expect(queue.elements).to(equal(["test"]))
      }

    }
  }

}
