//
//  PlaylistModelSpec.swift
//  Ham-SuppliesTests
//
//  Created by Mike Sabo on 5/22/18.
//  Copyright © 2018 Mike Sabo. All rights reserved.
//

import Foundation
import Quick
import Nimble
import RealmSwift

@testable import Ham_Supplies

class PlaylistModelSpec: QuickSpec {

  override func spec() {

    describe("Playlist Model") {

      var playlist: Playlist!
      var tracks: List<StoredTrack>! = List<StoredTrack>()
      var realm: Realm!
      let defaultName = "Sweet Playlist"

      beforeEach {
        Realm.Configuration.defaultConfiguration.inMemoryIdentifier = self.name
        realm = try! Realm(configuration: Realm.Configuration.defaultConfiguration)
        playlist = Playlist()
        playlist.name = defaultName
        tracks = List<StoredTrack>()
        StoredTrack.populate(inRealm: realm)
      }

      afterEach {
        try? realm.write {
          realm.deleteAll()
        }
      }

      it("should allow initialization with an empty array of tracks") {
        playlist = Playlist()
        let newName = "New Playlist"
        playlist.name = newName
        playlist.save(inRealm: realm)

        let playlists = realm.objects(Playlist.self)
        expect(playlists.count).to(equal(1))
        guard let first = playlists.first else {
          XCTFail("First playlist not unwrapped or found")
          return
        }
        expect(first.name).to(equal(newName))
        expect(first.name).notTo(equal(defaultName))
        expect(first.id).to(equal(1))
      }

      it("should allow tracks to be added after a playlist has been saved and retrieved from realm") {
        playlist.save(inRealm: realm)
        let firstTrack = StoredTrack.getFirst(inRealm: realm)
        playlist.addTrack(firstTrack, realm)
        let firstPlaylist = Playlist.getFirst(inRealm: realm)
        expect(firstPlaylist.tracks.count).to(equal(1))
        expect(firstPlaylist.tracks[0].title).to(equal("Catch Me (feat. Naaz)"))

      }

    }
  }

}

extension Playlist {

  public static func getFirst(inRealm realm: Realm) -> Playlist {
    guard let playlist = try? AssertNotNilAndUnwrap(realm.objects(Playlist.self).first) else {
      fail("Failing da test")
      return Playlist()
    }
    return playlist
  }

}
