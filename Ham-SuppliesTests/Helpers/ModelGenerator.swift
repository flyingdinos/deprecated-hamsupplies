//
//  ModelGenerator.swift
//  Ham-SuppliesTests
//
//  Created by Mike Sabo on 5/22/18.
//  Copyright © 2018 Mike Sabo. All rights reserved.
//

import Foundation
import Foundation
import Quick
import Nimble
import RealmSwift
import SwiftyJSON

@testable import Ham_Supplies

extension StoredTrack {

  public convenience init(trackWithName name: String) {
    self.init()
    self.title = name
  }
  
  @discardableResult
  public static func saveNew(withTitle title: String, inRealm realm: Realm) -> String {
    realm.beginWrite()
    let track = StoredTrack(json: TestData.jsonTracks["tracks"].arrayValue[0])
    track.title = title
    
    realm.add(track)
    try! realm.commitWrite()
    return track.id
  }
  
  public static func save(x: Int, inRealm realm: Realm) {
    realm.beginWrite()
    for n in 1...x {
      let track = StoredTrack(json: TestData.jsonTracks["tracks"].arrayValue[n])
      realm.add(track)
    }
    try! realm.commitWrite()
  }
  
  public static func populate(inRealm realm: Realm) {
    realm.beginWrite()
    TestData.jsonTracks["tracks"].arrayValue.forEach({ track in
      let track = StoredTrack(json: track)
      realm.add(track)
    })
    try! realm.commitWrite()
  }
  
  public static func getFirst(inRealm realm: Realm) -> StoredTrack {
    guard let track = try? AssertNotNilAndUnwrap(realm.objects(StoredTrack.self).first) else {
      fail("Failing da test")
      return StoredTrack()
    }
    return track
  }
  
}
